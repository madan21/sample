<?php

namespace app\modules\pl;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\pl\controllers';

    public function init()
    {
        Yii::$app->language = 'pl';
        Yii::setAlias('@mifid_questionnaire_views', "@app/modules/pl/views/mifid_questionnaire");

        parent::init();
    }
}