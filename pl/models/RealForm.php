<?php

namespace app\modules\pl\models;

use app\models\Form;
use app\modules\pl\components\helpers\CountryHelper;
use app\modules\pl\components\helpers\RealFormHelper;
use app\modules\pl\components\validators\SecondStepAgreements;
use app\modules\tms\TMS;
use Tms\Components\Shared\Antiflood;
use Tms\Models\Ws\ActivationData;
use Tms\Models\Ws\Mifid;
use Tms\Models\Ws\MifidQuestionnaire;
use Yii;
use app\modules\pl\components\validators\FirstStepAgreements;
use yii\base\Exception;
use yii\validators\CompareValidator;
use app\modules\pl\components\validators\Pesel;
use app\modules\pl\components\validators\DateOfBirth;
use app\modules\pl\components\validators\Citizenship;
use app\modules\pl\components\validators\PersonIdDocumentType;
use app\modules\pl\components\validators\PersonIdDocumentNumber;
use app\modules\pl\components\validators\ExpiryDateIdDocument;
use app\modules\pl\components\validators\TransferAccount;
use app\modules\pl\components\validators\FirstQuest;
use app\modules\pl\components\validators\AgreementsThirdStep;
use app\modules\pl\components\validators\TaxResident;

/**
 * Class RealForm
 * @package modules\pl\models
 *
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property string $compare_email
 * @property $agreements_first_step array
 * @property boolean $phoneVerified
 * @property string $postal_code
 * @property string $postal_code_contact
 * @property string $city
 * @property string $city_contact
 * @property string $street
 * @property string $street_contact
 * @property string $street_prefix
 * @property string $street_prefix_contact
 * @property string $house_number
 * @property string $house_number_contact
 * @property string $apartment_number
 * @property string $apartment_number_contact
 * @property string $country
 * @property string $country_contact
 * @property array $tax_resident
 * @property boolean $another_contact Checkbox if different address
 * @property boolean $pep Radio button 'political person (PEP)'
 * @property boolean $poland_tax_resident Radio button poland tax resident
 * @property integer $poland_tax_office_id If checked $poland_tax_resident
 * @property boolean $other_country_tax_resident Radio button "Other country tax resident"
 * @property string $country_tax_resident_value If checked "Other country tax resident"
 * @property string $tin Taxpayer Identification Number
 * @property boolean $not_have_tin Does not have Taxpayer Identification Number
 * @property string $not_have_tin_reason The reason for which has not $tin
 * @property boolean $usa_tax_resident
 * @property string $usa_tin Taxpayer Identification Number in USA
 * @property string $pesel
 * @property boolean $no_pesel Checkbox doesn't have pesel
 * @property string $date_of_birth If doesn't have pesel
 * @property string $citizenship
 * @property integer $person_id_document_type
 * @property string $person_id_document_number
 * @property string $expiry_date_person_id_document
 * @property boolean $no_limit_expiry_date
 * @property string $country_of_birth
 * @property string $city_of_birth
 * @property string $messaging_type
 * @property array $agreements_second_step
 * @property string $national_identification_number When isset national_identification_number in selected citizenship
 * @property array $first_quest
 * @property int $transfer_currency_id
 * @property int $account_type
 * @property string $phone_password
 * @property int $transfer_account
 * @property int $investment_goal
 * @property int $source_of_fund
 * @property string $other_country_tax_resident_value
 * @property array $agreements_third_step
 * @property boolean $ifDocumentTypeIsIdCard
 * @property boolean $issetNationalIdNumber
 * @property boolean $allowNoPesel
 * @property boolean $isPolandCountry
 * @property boolean $isOtherCountryTaxResident;
 * @property integer $upload_document_type
 * @property resource $upload_document_front_file
 * @property resource upload_document_back_file
 * @property integer $documentCounter
 * @property string $branch_code
 * @property string $language_code
 * @property array $backofficeClient
 *
 * @property string $phoneVerificationCode
 * @property string $phoneVerificationCode2
 */
class RealForm extends Form implements PatternsInterface
{
    //defaults
    const POLAND_COUNTRY_CODE = 'PL';
    const BUSINESS_ACTIVITY = 'NATURAL_PERSON';
    const DEFAULT_PROVINCE_ID = 17;
    const DEFAULT_POLAND_TAX_OFFICE_ID = 1076;

    //scenarios
    const SCENARIO_FIRST_STEP = 'step1';
    const SCENARIO_SECOND_STEP = 'step2';
    const SCENARIO_THIRD_STEP = 'step3';
    const SCENARIO_THIRD_STEP_POINTS = 'step3points';
    const SCENARIO_FOURTH_STEP = 'step4';
    const SCENARIO_STEP_ONE_PHONE = 'step1phone';
    const SCENARIO_STEP_ONE_PHONE_VERIFY = 'step1phoneVerify';

    public $isAuthenticated = false;
    public $password;
    public $documentCounter = 0;
    public $branch_code = 'PL';
    public $language_code = 'PL';

    //first step
    public $first_name;
    public $last_name;
    public $phone;
    public $email;
    public $compare_email;
    public $agreements_first_step = [];

    //second step
    public $postal_code;
    public $postal_code_contact;
    public $city;
    public $city_contact;
    public $street;
    public $street_contact;
    public $street_prefix;
    public $street_prefix_contact;
    public $house_number;
    public $house_number_contact;
    public $apartment_number;
    public $apartment_number_contact;
    public $country;
    public $country_contact;
    public $another_contact;
    public $pep;
    public $tax_resident = [];
    public $poland_tax_office_id;
    public $other_country_tax_resident_value;
    public $tin;
    public $not_have_tin;
    public $not_have_tin_reason;
    public $usa_tin;
    public $pesel;
    public $no_pesel = false;
    public $date_of_birth;
    public $citizenship;
    public $person_id_document_type;
    public $person_id_document_number;
    public $expiry_date_person_id_document;
    public $no_limit_expiry_date = false;
    public $country_of_birth;
    public $city_of_birth;
    public $messaging_type;
    public $agreements_second_step = [];
    public $national_identification_number;
    public $poland_tax_resident;
    public $other_country_tax_resident;
    public $usa_tax_resident;

    //third step
    public $first_quest = [];
    public $account_type = RealFormHelper::TMS_CONNECT_ACCOUNT_TYPE;
    public $transfer_currency_id;
    public $phone_password;
    public $transfer_account;
    public $investment_goal;
    public $source_of_fund;
    public $agreements_third_step;

    //third step points
    public $points = 0;
    public $pointsRecommendation;
    public $agreementInappropriate;
    public $complete = 0;

    //fourth step
    public $upload_document_type;
    public $upload_document_front_file;
    public $upload_document_back_file;

    //phone
    public $phoneVerified = false;
    public $phoneVerificationCode;
    public $phoneVerificationCode2;

    public $backofficeClient = [];

    public $webMobile;
    public $webLeadSource;

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_STEP_ONE_PHONE => [
                'phone',
                'phoneVerificationCode',
                'branch_code',
                'language_code'
            ],
            self::SCENARIO_STEP_ONE_PHONE_VERIFY => [
                'phoneVerified',
                'phoneVerificationCode',
                'phoneVerificationCode2'
            ],
            self::SCENARIO_FIRST_STEP => [
                'first_name',
                'last_name',
                'email',
                'compare_email',
                'phoneVerified',
                'agreements_first_step',
                'webMobile'
            ],
            self::SCENARIO_SECOND_STEP => [
                'tax_resident',
                'poland_tax_office_id',
                'other_country_tax_resident_value',
                'street',
                'street_contact',
                'street_prefix',
                'street_prefix_contact',
                'house_number',
                'house_number_contact',
                'postal_code',
                'postal_code_contact',
                'city',
                'city_contact',
                'country',
                'country_contact',
                'pesel',
                'no_pesel',
                'date_of_birth',
                'apartment_number',
                'apartment_number_contact',
                'another_contact',
                'pep',
                'tin',
                'not_have_tin',
                'not_have_tin_reason',
                'usa_tin',
                'citizenship',
                'person_id_document_type',
                'person_id_document_number',
                'expiry_date_person_id_document',
                'no_limit_expiry_date',
                'country_of_birth',
                'city_of_birth',
                'messaging_type',
                'agreements_second_step',
                'national_identification_number',
                'poland_tax_resident',
                'other_country_tax_resident',
                'usa_tax_resident',
            ],
            self::SCENARIO_THIRD_STEP => [
                'first_quest',
                'transfer_currency_id',
                'phone_password',
                'transfer_account',
                'investment_goal',
                'source_of_fund',
                'agreements_third_step',
            ],
            self::SCENARIO_THIRD_STEP_POINTS => [
                'first_quest',
                'transfer_currency_id',
                'phone_password',
                'transfer_account',
                'investment_goal',
                'source_of_fund',
                'agreements_third_step',
                'points',
                'pointsRecommendation',
                'agreementInappropriate',
                'complete'
            ],
            self::SCENARIO_FOURTH_STEP => [
                'upload_document_front_file',
                'upload_document_back_file',
            ],
        ];
    }

    public function rules()
    {
        $isNotAuthenticated = function ($model, $attribute)
        {
            /** @var RealForm $model */
            return ! $model->isAuthenticated;
        };

        $acceptAgreementInappopriate = function ($model, $attribute)
        {
            return $model->pointsRecommendation == Mifid::RECOMENDATION_INAPPROPRIATE;
        };
        $uploadDocumentBackFile = function ($model, $attribute)
        {
            return Yii::$app->processPoland->realForm->person_id_document_type == RealFormHelper::IDENTITY_CARD_PERSONAL_DOCUMENT;
        };

        return [
            [['password', 'branch_code', 'language_code', 'backofficeClient'], 'safe'],
            ['documentCounter', 'integer'],
            //step1 phone verification
            [['phone'], 'required', 'on' => self::SCENARIO_STEP_ONE_PHONE, 'message' => 'Wypełnienie pola "Telefon" jest wymagane.'],
            [['phone'], 'match', 'pattern' => self::PHONE_PATTERN, 'on' => self::SCENARIO_STEP_ONE_PHONE,
                'message' => Yii::t('app', 'Invalid phone.')],
            ['phoneVerificationCode2', 'required', 'on' => self::SCENARIO_STEP_ONE_PHONE_VERIFY, 'message' => Yii::t('app', 'SMS code doesn`t match.')],
            ['phoneVerificationCode2', 'compare', 'compareAttribute' => 'phoneVerificationCode', 'on' => self::SCENARIO_STEP_ONE_PHONE_VERIFY,
                'operator' => '===', 'message' => Yii::t('app', 'SMS code doesn`t match.')],
            //first step
            [['first_name', 'last_name', 'phoneVerified'], 'required'],
            [['first_name', 'last_name'], 'match', 'pattern' => self::NAME_STRING_PATTERN],
            [['email', 'compare_email'], 'required', 'when' => $isNotAuthenticated],
            [['email'], 'email', 'when' => $isNotAuthenticated],
//            [['email'], 'identifyEmail', 'on' => 'step1', 'when' => $isNotAuthenticated],
            ['compare_email', 'compare', 'compareAttribute' => 'email', 'when' => $isNotAuthenticated, 'message' => 'Email doesn\'t match'],
            ['agreements_first_step', FirstStepAgreements::class, 'skipOnEmpty' => false],
            ['phoneVerified', 'isPhoneVerified', 'on' => self::SCENARIO_FIRST_STEP],

            //second step
            [[
                'postal_code', 'city', 'street', 'house_number', 'country', 'house_number', 'citizenship', 'country_of_birth', 'city_of_birth', 'messaging_type',
                'poland_tax_resident', 'other_country_tax_resident', 'usa_tax_resident', 'person_id_document_type',
            ], 'required'],
            [['postal_code_contact', 'city_contact', 'street_contact', 'house_number_contact', 'country_contact'],
                'required', 'when' => function (RealForm $model)
            {
                return ($model->another_contact);
            }],
            [['country', 'country_contact', 'other_country_tax_resident_value', 'citizenship', 'country_of_birth',],
                'in', 'range' => CountryHelper::getKeysList()],
            [['street_prefix', 'street_prefix_contact', 'apartment_number', 'apartment_number_contact'], 'safe'],
            ['tax_resident', 'each', 'rule' => ['in', 'range' => RealFormHelper::$taxResidents]],
            ['tax_resident', TaxResident::class, 'poland_tax_office_id' => 'poland_tax_office_id', 'other_country_tax_resident_value' => 'other_country_tax_resident_value',
                'tin' => 'tin', 'not_have_tin' => 'not_have_tin', 'not_have_tin_reason' => 'not_have_tin_reason', 'usa_tin' => 'usa_tin', 'skipOnEmpty' => false],
            ['usa_tin', 'match', 'pattern' => self::USA_TIN_PATTERN, 'message' => 'Numer TIN powinien składać się z 9 cyfr.'],
            [['postal_code', 'postal_code_contact'], 'match', 'pattern' => self::POSTAL_CODE_PATTERN, 'when' => function (RealForm $model)
            {
                return ($this->country == self::POLAND_COUNTRY_CODE);
            }, 'message' => 'Podaj kod pocztowy w formacie xx-xxx'],
            ['pesel', 'required', 'when' => function (RealForm $model)
            {
                return ! $model->no_pesel;
            }, 'message' => "Podano zły PESEL"],
            ['pesel', Pesel::class, 'citizenship' => 'citizenship', 'no_pesel' => 'no_pesel'],
            ['date_of_birth', 'date', 'format' => 'Y-m-d'],
            ['date_of_birth', DateOfBirth::class, 'no_pesel' => 'no_pesel'],
            ['pep', 'in', 'range' => [RealFormHelper::YES_ANSWER_WORD, RealFormHelper::NO_ANSWER_WORD]],
            ['another_contact', 'boolean'],
            ['citizenship', Citizenship::class, 'national_identification_number' => 'national_identification_number'],
            ['person_id_document_type', PersonIdDocumentType::class, 'citizenship' => 'citizenship', 'skipOnEmpty' => false],
            ['person_id_document_number', PersonIdDocumentNumber::class, 'citizenship' => 'citizenship', 'person_id_document_type' => 'person_id_document_type'],
            ['expiry_date_person_id_document', 'required', 'when' => function (RealForm $model)
            {
                return ! $model->no_limit_expiry_date;
            }],
            ['expiry_date_person_id_document', 'date', 'format' => 'Y-m-d', 'message' => 'Błędnie wprowadzona data', 'skipOnEmpty' => true],
            ['expiry_date_person_id_document', ExpiryDateIdDocument::class, 'no_limit_expiry_date' => 'no_limit_expiry_date'],
            ['messaging_type', 'in', 'range' => RealFormHelper::getMessagingTypes()],
            ['agreements_second_step', SecondStepAgreements::class, 'skipOnEmpty' => false],
            [['poland_tax_resident', 'other_country_tax_resident', 'usa_tax_resident'], 'boolean'],
            //third_step
            [['transfer_currency_id', 'phone_password', 'transfer_account', 'investment_goal', 'source_of_fund', 'first_quest', 'account_type'], 'required'],
            ['account_type', 'in', 'range' => RealFormHelper::getTmsAccountTypes()],
            ['transfer_currency_id', 'in', 'range' => RealFormHelper::getDepositCurrencies()],
            ['investment_goal', 'in', 'range' => RealFormHelper::getInvestmentGoalTypes()],
            ['source_of_fund', 'in', 'range' => RealFormHelper::getSourcesOfFoundations()],
            ['transfer_account', TransferAccount::class],
            ['first_quest', FirstQuest::class],
            ['agreements_third_step', AgreementsThirdStep::class, 'skipOnEmpty' => false],
            //step3points
            [['complete'], 'compare', 'operator' => '==', 'compareValue' => '1', 'message' => 'Invalid value'],
            [['pointsRecommendation'], 'in', 'range' => [Mifid::RECOMENDATION_INAPPROPRIATE, Mifid::RECOMENDATION_APPROPRIATE], 'message' => 'Invalid recommendation.'],
            [['agreementInappropriate'], 'required', 'on' => 'step3points', 'message' => Yii::t('app', 'This field is required.'), 'when' => $acceptAgreementInappopriate],
            [['agreementInappropriate'], 'compare', 'operator' => '==', 'compareValue' => 1, 'message' => Yii::t('app', 'You must agree to terms & conditions.'), 'when' => $acceptAgreementInappopriate],
            //fourth step
            [['upload_document_front_file'], 'required', 'message' => 'Prześlij zdjęcie dokumentu'],
            [['upload_document_back_file'], 'required', 'when' => $uploadDocumentBackFile, 'message' => 'Prześlij zdjęcie dokumentu'],
            //['upload_document_type', 'in', 'range' => RealFormHelper::getDocumentTypes()],
            [['upload_document_front_file', 'upload_document_back_file'], 'file', 'extensions' => 'png, jpg, pdf'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->addingPrefixToPhoneAndTrim();
        $this->prepareStringDataInSecondStep();
        $this->prepareStringDataInThirdStep();
        $this->setTaxResidentValue();
        return parent::beforeValidate();
    }

    /**
     * @return bool
     */
    protected function addingPrefixToPhoneAndTrim()
    {
        if ($this->scenario != self::SCENARIO_STEP_ONE_PHONE)
        {
            return true;
        }

        $this->phone = trim(strlen($this->phone) == 9 ? '+48' . $this->phone : $this->phone);
    }

    /**
     * @return bool
     */
    protected function prepareStringDataInSecondStep()
    {
        if ($this->scenario != self::SCENARIO_SECOND_STEP)
        {
            return true;
        }

        $this->replaceSpaces([
            'street',
            'street_contact',
            'house_number',
            'house_number_contact',
            'postal_code',
            'postal_code_contact',
            'city',
            'city_contact',
            'country',
            'country_contact'
        ]);

        $this->uppercaseFirstLetter([
            'city',
            'city_contact',
            'street',
            'street_contact',
        ]);
    }

    /**
     * @return bool
     */
    protected function prepareStringDataInThirdStep()
    {
        if ($this->scenario != self::SCENARIO_THIRD_STEP)
        {
            return true;
        }

        $this->transfer_account = preg_replace(self::NIP_PATTERN, '', $this->transfer_account);
    }

    /**
     * Set tax resident value
     */
    protected function setTaxResidentValue()
    {
        if ( ! $this->poland_tax_resident)
        {
            $this->poland_tax_office_id = self::DEFAULT_POLAND_TAX_OFFICE_ID;
        }

        if ($this->poland_tax_resident)
        {
            array_push($this->tax_resident, RealFormHelper::TAX_RESIDENT_POLAND);
        }

        if ($this->other_country_tax_resident)
        {
            array_push($this->tax_resident, RealFormHelper::TAX_RESIDENT_OTHER_COUNTRY);
        }
        if ($this->usa_tax_resident)
        {
            array_push($this->tax_resident, RealFormHelper::TAX_RESIDENT_USA);
        }
    }

    protected function replaceSpaces($attributes)
    {
        foreach ($attributes as $attributeName)
        {
            $this->$attributeName = preg_replace(self::REPLACE_SPACES_PATTERN, '', $this->$attributeName);
        }
    }

    protected function uppercaseFirstLetter($attributes)
    {
        foreach ($attributes as $attributeName)
        {
            $this->$attributeName = mb_convert_case($this->$attributeName, MB_CASE_TITLE, 'UTF-8');
        }
    }

    /**
     * Walidacja pliku - czy został już wczytany
     * @param $attribute
     * @param $params
     */
    public function isPhoneVerified($attribute, $params)
    {
        if ( ! $this->hasErrors('phone') && ! $this->{$attribute} === true)
        {
            $this->addError('phone', Yii::t('app', 'Please verify your phone'));
        }
    }

    /**
     * @param array|string $names
     * @param array|string $value
     */
    public function singleValidation($names, $value)
    {
        if ( ! is_array($names))
        {
            $names = [$names];
        }

        foreach ($names as $i => $name)
        {
            if ($this->hasProperty($name))
            {
                $this->$name = $value;
                $this->branch_code = Yii::$app->request->post('branch_code');
                $this->language_code = Yii::$app->request->post('language_code');

                foreach ($this->getAllScenariosForValidation() as $scenario => $fields)
                {
                    if (in_array($name, $fields))
                    {
                        $this->setScenario($scenario);
                        break;
                    }
                }
                if ($this->scenario == 'default')
                {
                    $this->addError($name, 'Invalid attribute');
                }
                else
                {
                    foreach ($this->getActiveValidators($name) as $validator)
                    {
                        if ($validator instanceof CompareValidator && isset($validator->compareAttribute))
                        {
                            $this->{$validator->compareAttribute} = Yii::$app->request->post('compare');
                        }
                    }
                    $this->validate([$name], false);
                }
            }
        }
    }

    /**
     * @return array
     */
    protected function getAllScenariosForValidation()
    {
        $scenarios = $this->scenarios();

        return $scenarios;
    }

    /**
     * @return bool
     */
    public function getIsPolandCountry()
    {
        return ($this->country == self::POLAND_COUNTRY_CODE);
    }

    /**
     * @return bool
     */
    public function getIfDocumentTypeIsIdCard()
    {
        return ($this->person_id_document_type == RealFormHelper::IDENTITY_CARD_PERSONAL_DOCUMENT);
    }

    /**
     * @return bool
     */
    public function getIssetNationalIdNumber()
    {
        return ! empty(CountryHelper::getNationalIdentificationNumberLabel($this->citizenship));
    }

    /**
     * @return bool
     */
    public function getAllowNoPesel()
    {
        return ($this->no_pesel && ! empty($this->date_of_birth));
    }

    /**
     * @return bool
     */
    public function getIsOtherCountryTaxResident()
    {
        return in_array(RealFormHelper::TAX_RESIDENT_OTHER_COUNTRY, $this->tax_resident);
    }

    /**
     * @return bool
     */
    public function getNoTinReasonIsOther()
    {
        return ($this->not_have_tin_reason == 'other');
    }

    /**
     * @param string $key
     * @return string|null
     */
    public function getFirstQuestValueByKey($key)
    {
        if(isset($this->first_quest[$key]) && !empty($this->first_quest[$key])) {
            return $this->first_quest[$key];
        }

        return null;
    }

    /**
     * @param string $attribute
     * @param array $params
     */
//    public function identifyEmail($attribute, $params)
//    {
//        /* @var TMS $tms */
//        $tms = Yii::$app->getModule('tms');
//        $response = $tms->clientZone->IdentifyEmail($this->$attribute, null, 'tms');
//        if($response->success()) {
//            $responseProfileInfo = $tms->clientZone->GetProfileInfo($response->profileId);
//            //Email validation for real account only
//            if($responseProfileInfo->type == "REAL" || $responseProfileInfo->type == "REAL_PLUS") {
//                $url = 'https://en.tmseurope.com/newpassword';
//                if($this->branch_code != 'eu') {
//                    $url = 'https://www.tmseurope.com/newpassword';
//                    $url = str_replace('com', strtolower($this->branch_code), $url);
//                    if($this->branch_code != $this->languageCode) {
//                        $url = str_replace('newpassword', strtolower($this->languageCode) . '/newpassword', $url);
//                    }
//                }
//                $this->addError($attribute, Yii::t('app', 'That e-mail is already used. Login or reset your password <a href="{url}">here</a>.', ['url' => $url]));
//            }
//        }
//    }

    /**
     * @return string
     */
    public function getLeadSourceSuffix()
    {
        if (isset($this->webLeadSource) && ! empty($this->webLeadSource))
        {
            $result = $this->webLeadSource;
        }
        else
        {
            $result = $this->webMobile ? '_mobile' : '';
        }
        return $result;
    }
}