<?php

namespace  app\modules\pl\models;


interface PatternsInterface
{
    const POSTAL_CODE_PATTERN = '/^\d{2}-\d{3}$/';
    const NAME_STRING_PATTERN = '/^[\-\sa-zA-Z\x{00C0}-\x{00D6}\x{00D8}-\x{00F6}\x{00F8}-\x{00FF}\x{0100}-\x{017F}\x{0180}-\x{024F}]+$/u';
    const PHONE_PATTERN = '/^[0-9\.\(\)\s\+]{9,18}$/';
    const REPLACE_SPACES_PATTERN = '/\s/';
    const NIP_PATTERN = '/([^\dA-Z])/';
    const USA_TIN_PATTERN = '/^[0-9]{9}$/';
    const NATIONAL_ID_NUMBER_PATTERN = '/^[A-Z0-9]+$/';
    const DOCUMENT_NUMBER_PATTERN = '/[^a-zA-Z0-9]*/';
    const DATE_OF_BIRTH_PATTERN = '/^([\d]{2})\-([\d]{2})\-([\d]{4})$/';
}