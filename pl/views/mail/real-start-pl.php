<table width="100%" border="0">
    <tbody>
    <tr>
        <td align="center">
            <table width="682" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial">
                <tbody>
                <tr>
                    <td colspan="3" style="height: 27px">&nbsp;</td>
                </tr>
                <tr>
                    <td width="30">&nbsp;</td>
                    <td style="text-align: left">
                        <img src="http://www.tms-alpha.pl/sites/all/themes/tms/images/mail/logo.png?v=1"
                             alt="TMS Brokers" height="39" width="215" style="display: block; border: 0px"></td>
                    <td width="30">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 20px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <p>Użytkownik rozpoczął wypełnianie formularza <?= $data['name'] ?> </p>
                        <p>Dane:</p>
                        <p><strong>Imię</strong>: <?= $data['first_name'] ?></p>
                        <p><strong>Nazwisko</strong>: <?= $data['last_name'] ?></p>
                        <p><strong>Email</strong>: <a href="mailto:<?= $data['user_email'] ?>"
                                                      onclick="return rcmail.command('compose',<?= $data['user_email'] ?>,this)"
                                                      rel="noreferrer"><?= $data['user_email'] ?></a></p>
                        <p><strong>Telefon</strong>: <?= $data['phone'] ?></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 27px">&nbsp;</td>
                </tr>
                <tr>
                    <td width="30">&nbsp;</td>
                    <td style="color: #abafba; text-align: center; font-size: 12px; line-height: 1.4; font-family: Arial">
                        Niniejsza wiadomość została przesłana przez Dom&nbsp;Maklerski TMS&nbsp;Brokers Spółka Akcyjna z&nbsp;siedzibą
                        w&nbsp;Warszawie, ulica Złota&nbsp;59 00-120 Warszawa zarejestrowany przez Sąd Rejonowy dla m.&nbsp;st.&nbsp;Warszawy
                        w&nbsp;Warszawie, XII Wydział Gospodarczy Krajowego Rejestru Sądowego pod numerem KRS&nbsp;0000204776,
                        NIP&nbsp;526-27-59-131, kapitał zakładowy 3&nbsp;537&nbsp;560 złotych opłacony w&nbsp;całości.
                    </td>
                    <td width="30">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 27px">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
