<?php

namespace app\modules\pl\controllers;

use app\components\Request;
use app\controllers\AppController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\modules\pl\models\RealForm;
use app\modules\pl\components\TmsAccount;

/**
 * Class RealController
 * @package app\modules\pl\controllers
 *
 * @property Request $request
 * @property TmsAccount $tmsAccount
 */
class RealController extends AppController
{
    /**
     * @var TmsAccount
     */
    protected $tmsAccount;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $result = parent::beforeAction($action);

        $this->tmsAccount = new TmsAccount(new RealForm());
        $this->request = Yii::$app->request;

        $this->response = $this->tmsAccount->validateAuthToken($this->request);

        if(!$this->response['validate_token']) {
            Yii::$app->response->setStatusCode(403);
            $result = false;
            echo Json::encode($this->response);
        }
        return $result;
    }

    /**
     * @return array
     */
    public function actionSendSms()
    {
        $this->tmsAccount->setRealFormPropertyValue('phone',  $this->request->post('phone'));
        $this->setResponse($this->tmsAccount->sendSms());

        return $this->response;
    }

    /**
     * @return array
     */
    public function actionValidateSms()
    {
        $this->tmsAccount->loadPostData(RealForm::SCENARIO_STEP_ONE_PHONE_VERIFY, $this->request->post());
        $this->setResponse($this->tmsAccount->validateSms($this->request->post('token')));

        return $this->response;
    }

    /**
     * @return array
     */
    public function actionValidate()
    {
        return $this->tmsAccount->validateProperty($this->request->post('name', []), $this->request->post('value', []));
    }

    /**
     * @return array
     */
    public function actionStep1()
    {
        $this->tmsAccount->loadPostData(RealForm::SCENARIO_FIRST_STEP, $this->request->post());
        $this->setResponse($this->tmsAccount->saveFirstStepData($this->request));

        return $this->response;
    }

    /**
     * @return array
     */
    public function actionStep2()
    {
        $this->tmsAccount->loadPostData(RealForm::SCENARIO_SECOND_STEP, $this->request->post());
        $this->setResponse($this->tmsAccount->saveSecondStepData($this->request->post('token')));

        return $this->response;
    }

    /**
     * @return array
     */
    public function actionStep3()
    {
        $this->tmsAccount->loadPostData(RealForm::SCENARIO_THIRD_STEP, $this->request->post());
        $this->setResponse($this->tmsAccount->saveThirdStepData($this->request->post('token')));

        return $this->response;
    }

    /**
     * @return array
     */
    public function actionStep4()
    {
        $this->tmsAccount->loadPostData(RealForm::SCENARIO_FOURTH_STEP, $this->request->post());
        $this->setResponse($this->tmsAccount->uploadDocumentFiles($this->request->post('token')));

        return $this->response;
    }

    /**
     * @return array
     */
    public function actionGetDictionariesList()
    {
        return $this->tmsAccount->getDictionariesList();
    }

    /**
     * @param string $postal_code
     * @param string|null $city
     * @return array
     */
    public function actionGetProvinceAndTaxOffice($postal_code, $city = null)
    {
        return $this->tmsAccount->getProvinceAndTaxOffice($postal_code, $city);
    }

    /**
     * @param array $methodResponse
     */
    protected function setResponse($methodResponse)
    {
        if($methodResponse['method_result']) {
            $this->setSuccess($methodResponse);
        } else {
            $this->setErrorResponse($methodResponse);
        }
    }

    /**
     * Sets common values for success response
     * @param array $otherResponseAttributes
     */
    protected function setSuccess($otherResponseAttributes = [])
    {
        $this->response = ArrayHelper::merge($this->response, [
            'token' => Yii::$app->processPoland->getToken(Yii::$app->request->getUserIP()),
            'status' => 'success',
            'info' => [
                'Activation Id' => Yii::$app->processPoland->activationId,
                'Profile Id' => Yii::$app->processPoland->profileId,
                'Client Id' => Yii::$app->processPoland->clientId,
                'Email' => Yii::$app->processPoland->realForm->email,
            ],
        ]);

        if(!empty(Yii::$app->processPoland->activationId) && Yii::$app->processPoland->realForm->email) {
            $this->response['leadId'] = md5(Yii::$app->processPoland->activationId);
            $this->response['userId'] = md5(Yii::$app->processPoland->realForm->email);
        }

        $this->response = ArrayHelper::merge($this->response, $otherResponseAttributes);
    }

    /**
     * Sets common values for error response
     * @param array $otherResponseAttributes
     */
    protected function setErrorResponse($otherResponseAttributes = [])
    {
        $this->response = [
            'status' => 'error',
            'info' => [
                'Activation Id' => Yii::$app->processPoland->activationId,
                'Profile Id' => Yii::$app->processPoland->profileId,
                'Client Id' => Yii::$app->processPoland->clientId,
                'Email' => Yii::$app->processPoland->realForm->email,
            ],
        ];

        $this->response = ArrayHelper::merge($this->response, $otherResponseAttributes);
    }

    public function actionTest()
    {

    }
}