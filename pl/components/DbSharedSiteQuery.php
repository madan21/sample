<?php

namespace app\modules\pl\components;

use app\modules\pl\models\RealForm;
use yii\base\Component;
use Yii;
use yii\db\Connection;

/**
 * Class DbSharedSiteQuery
 * @package modules\pl\components
 *
 * @property Connection $dbConnection
 */
class DbSharedSiteQuery extends Component
{
    /**
     * @var Connection
     */
    protected $dbConnection;

    /**
     * @inheritdoc
     */
    public function init(){

        $this->dbConnection = new Connection([
            'dsn' => Yii::$app->components["shared-site"]["dsn"],
            'username' => Yii::$app->components["shared-site"]["username"],
            'password' => Yii::$app->components["shared-site"]["password"],
            'charset' => Yii::$app->components["shared-site"]["charset"],
        ]);
        $this->dbConnection->open();

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function __destruct(){
        $this->closeConnection();
    }

    /**
     * @return bool
     */
    public function closeConnection()
    {
        if($this->dbConnection->isActive) {
            return $this->dbConnection->close();
        }

        return true;
    }

    /**
     * @param string $city
     * @return array|null
     */
    public function getTaxOfficeList($city = null)
    {
        if(is_null($city)) {
            $response = $this->getAllTaxAddresses();
        } else {
            $response = $this->getTaxListByCity($city);
        }

        return $this->getStructuralAddressesResponse($response);
    }

    /**
     * @param string $postalCode
     * @param string $city
     * @return array|bool
     */
    public function getFullAddressByPostalCodeAndCity($postalCode, $city)
    {
        $city = trim($city);
        if (preg_match(RealForm::POSTAL_CODE_PATTERN, $postalCode)) {
            $queryResult = $this->getPostalCodeQuery($postalCode);

            if (count($queryResult) == 1) {
                return $queryResult[0];
            } elseif (count($queryResult) > 1) {

                foreach ($queryResult as $key => $row) {
                    if ($row['city'] == $city) {
                        break;
                    }
                }

                return (isset($key) && isset($queryResult[$key]) && !empty($queryResult[$key])) ? $queryResult[$key] : false;
            }
        }

        return false;
    }

    /**
     * @param string $postalCode
     * @return array|null
     */
    public function getPostalCodeQuery($postalCode)
    {
        return $this->dbConnection
            ->createCommand("SELECT * FROM postal_codes WHERE postal_code = :postalcode")
            ->bindValue(':postalcode', $postalCode)
            ->queryAll();
    }

    /**
     * @return array|null
     */
    protected function getAllTaxAddresses()
    {
        return $this->dbConnection
            ->createCommand("SELECT id, name as city, REPLACE(address, city, '') as address FROM us ORDER BY city ASC")
            ->queryAll();
    }

    /**
     * @param string $city
     * @return array|null
     */
    protected function getTaxListByCity($city)
    {
        return $this->dbConnection
            ->createCommand("SELECT id, name as city, REPLACE(address, city, '') as address FROM us WHERE TRIM(LOWER(city)) = :city OR id = 1076 ORDER BY address ASC")
            ->bindValue(':city', trim($city))
            ->queryAll();
    }


    /**
     * @param array $response
     * @return array
     */
    protected function getStructuralAddressesResponse($response)
    {
        $structuralResponse = [];

        if(!empty($response)) {
            foreach ($response as $row) {
                $structuralResponse[$row['id']] = trim($row['city'] . ' - ' . $row['address']);
            }
        }

        return $structuralResponse;
    }

    /**
     * @param string $postal_code
     * @return int
     */
    public function getProvinceId($postal_code)
    {
        return $this->dbConnection
            ->createCommand("SELECT * FROM provinces as p RIGHT JOIN postal_codes AS pc ON LOWER(CONVERT(p.name USING utf8 )) = CONVERT(pc.province USING utf8 ) WHERE pc.postal_code = :pc")
            ->bindValue(':pc', $postal_code)
            ->queryScalar();

//        $province_id = empty($response['id']) ? 17 : $response['id'];
//
//        /*
//         * maciek 20121102, część rachunków real ma błędny id province
//         */
//        if ($province_id > 17) {
//            $province_id = 17;
//        }
//        return $province_id;
    }

    public function getTest($postalCode)
    {
        return $this->dbConnection
            ->createCommand("SELECT * FROM postal_codes WHERE postal_code = :postalcode")
            ->bindValue(':postalcode', $postalCode)
            ->queryAll();
    }
}