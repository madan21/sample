<?php

namespace app\modules\pl\components;

use app\components\Process;
use app\modules\pl\models\RealForm;
use Yii;
use yii\base\Component;

class ProcessPoland extends Process
{
    /**
     * @param array $config
     * @param RealForm $realForm
     */
    public function __construct($config = [], RealForm $realForm)
    {
        $this->_realForm = $realForm;
        Component::__construct($config);
    }
}