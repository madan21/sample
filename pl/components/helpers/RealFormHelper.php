<?php

namespace app\modules\pl\components\helpers;

use Yii;
use Tms\Models\Ws\MifidQuestionnaireV2;

class RealFormHelper
{
    //document types
    const IDENTITY_CARD_PERSONAL_DOCUMENT = 1;
    const PASSPORT_PERSONAL_DOCUMENT = 2;

    //messaging types
    const EMAIL_MESSAGING_TYPE = 5;
    const POST_MESSAGING_TYPE = 6;

    //tms account types
    const TMS_TRADER_ACCOUNT_TYPE = 2;
    const TMS_CONNECT_ACCOUNT_TYPE = 4;
    const TMS_CONNECT_PREMIUM_ACCOUNT_TYPE = 6;
    const TMS_DIRECT_ACCOUNT_TYPE = 3;

    //investment goal types
    const INV_GOAL_INVESTING_FINANCIAL = 1;
    const INV_GOAL_REDUCING_EXCHANGE_RISK = 2;
    const INV_GOAL_INCREASE_ASSETS = 3;

    //sources of fund
    const FUND_CONTRACT = 1;
    const FUND_FINANCIAL_TRANSACT = 2;
    const FUND_GIFT = 3;
    const FUND_ECONOMIC_ACTIVITY = 4;
    const FUND_PENSION_OR_RENT = 5;
    const FUND_PROF_SERVICE = 6;
    const FUND_RENT_OR_LEASE = 7;
    const FUND_LACK_INCOME = 8;

    //deposit_currencies
    const DEPOSIT_CURRENCY_PLN = 1;
    const DEPOSIT_CURRENCY_EUR = 2;
    const DEPOSIT_CURRENCY_USD = 3;

    //questions
    const QUESTION_HAVE_EDUCATION = 'HaveEducation';
    const QUESTION_WORK_EXPERIENCE = 'WorkExperience';
    const QUESTION_PLATFORM_EXPERIENCE = 'PlatformExperience';
    const QUESTION_ORGANIZED_MARKET_TRADES = 'OrganizedMarketTrades';
    const QUESTION_LEVERAGE_MARKET_TRADES = 'LeveragedMarketTrades';
        //sub question for LeveragedMarketTrades(Value not passed if "NO_TRADES")
        const QUESTION_AVERAGE_TRANSACTION_VALUE = 'AverageTransactionValue';
        const QUESTION_NATURE_OF_INVESTMENT = 'NatureOfInvestment';
    const QUESTION_ORDER_TYPE_QUESTION = 'OrderTypeQuestion';
    const QUESTION_INVEST_RESULT_QUESTION = 'InvestmentResultQuestion';
    const QUESTION_HAVE_LOSS_AWARENESS = 'HaveLossAwareness';
    const QUESTION_PURPOSE_OF_INVESTMENT = 'PurposeOfInvestment';
    const QUESTION_INVEST_PERCENTAGE_OF_SAVING = 'InvestPercentOfSavings';
    const QUESTION_ACCEPT_RISK_OF_LOSS = 'AcceptRiskOfLoss';

    //boolean answers
    const YES_ANSWER = 1;
    const NO_ANSWER = 0;
    const YES_ANSWER_WORD = 'YES';
    const NO_ANSWER_WORD = 'NO';

    //tax resident
    const TAX_RESIDENT_POLAND = 1;
    const TAX_RESIDENT_OTHER_COUNTRY = 2;
    const TAX_RESIDENT_USA = 3;

    /**
     * @var array
     */
    public static $taxResidents = [
        self::TAX_RESIDENT_POLAND,
        self::TAX_RESIDENT_OTHER_COUNTRY,
        self::TAX_RESIDENT_USA,
    ];

    /**
     * @return array
     */
    public static function getBooleanAnswerLabels()
    {
        return [
            self::YES_ANSWER => 'tak',
            self::NO_ANSWER => 'nie',
        ];
    }

    /**
     * @return array
     */
    public static function getDepositCurrencies()
    {
        return array_keys(self::getDepositCurrencyLabels());
    }

    /**
     * @return array
     */
    public static function getDepositCurrencyLabels()
    {
        return [
            self::DEPOSIT_CURRENCY_PLN => 'PLN',
            self::DEPOSIT_CURRENCY_EUR => 'EUR',
            self::DEPOSIT_CURRENCY_USD => 'USD',
        ];
    }

    /**
     * @return array
     */
    public static function getSourcesOfFoundations()
    {
        return array_keys(self::getSourcesOfFundLabels());
    }

    /**
     * @return array
     */
    public static function getSourcesOfFundLabels()
    {
        return [
            self::FUND_CONTRACT => 'Umowa o pracę, zlecenie, umowy o dzieło, inne podobne',
            self::FUND_FINANCIAL_TRANSACT => 'Transakcje na rynku finansowym',
            self::FUND_GIFT => 'Spadek, darowizna, wygrana losowa',
            self::FUND_ECONOMIC_ACTIVITY => 'Działalność gospodarcza',
            self::FUND_PENSION_OR_RENT => 'Emerytura lub renta',
            self::FUND_PROF_SERVICE => 'Wykonywanie wolnego zawodu',
            self::FUND_RENT_OR_LEASE => 'Najem, dzierżawa lub inne przychody z nieruchomości',
            self::FUND_LACK_INCOME => 'Brak dochodów',
        ];
    }

    /**
     * @return array
     */
    public static function getInvestmentGoalTypes()
    {
        return array_keys(self::getInvestmentGoalTypeLabels());
    }

    /**
     * @return array
     */
    public static function getInvestmentGoalTypeLabels()
    {
        return [
            self::INV_GOAL_INVESTING_FINANCIAL => 'Inwestowanie nadwyżek finansowych',
            self::INV_GOAL_REDUCING_EXCHANGE_RISK => 'Ograniczenie ryzyka kursowego',
            self::INV_GOAL_INCREASE_ASSETS => 'Wzrost wartości aktywów',
        ];
    }

    /**
     * @return array
     */
    public static function getTmsAccountTypeLabels()
    {
        return [
            self::TMS_TRADER_ACCOUNT_TYPE => 'TMS Trader',
            self::TMS_CONNECT_ACCOUNT_TYPE => 'TMS Connect',
            self::TMS_CONNECT_PREMIUM_ACCOUNT_TYPE => 'TMS Connect Premium',
            self::TMS_DIRECT_ACCOUNT_TYPE => 'TMS Direct',
        ];
    }

    /**
     * @return array
     */
    public static function getTmsAccountTypes()
    {
        return array_keys(self::getTmsAccountTypeLabels());
    }

    /**
     * @return array
     */
    public static function getDocumentTypeLabels()
    {
        return [
            self::IDENTITY_CARD_PERSONAL_DOCUMENT => 'Dowód osobisty',
            self::PASSPORT_PERSONAL_DOCUMENT => 'Paszport',
        ];
    }

    /**
     * @return array
     */
    public static function getDocumentTypes()
    {
        return array_keys(self::getDocumentTypeLabels());
    }

    /**
     * @return array
     */
    public static function getMessagingTypeLabels()
    {
        return [
            self::EMAIL_MESSAGING_TYPE => 'Email (bezpłatnie)',
            self::POST_MESSAGING_TYPE =>'Listownie w formie drukowanej (tryb, który może wiązać się w przypadkach wskazanych w Tabeli Opłat i Prowizji z dodatkową opłatą na rzecz TMS Brokers',
        ];
    }

    /**
     * @return array
     */
    public static function getMessagingTypes()
    {
        return array_keys(self::getMessagingTypeLabels());
    }

    /**
     * @param string $templateName
     * @return bool|string
     */
    protected static function includeMifidTemplate($templateName)
    {
        return file_get_contents(\Yii::getAlias('@mifid_questionnaire_views') . "/{$templateName}.html", true);
    }

    /**
     * @return array
     */
    public static function getMifidQuestionnaireStructure()
    {
        $mifidQuestionnaire = new MifidQuestionnaireV2();
        return [
            self::QUESTION_HAVE_EDUCATION => [
                'answers' => ['YES','NO'],
                //'label' => self::includeMifidTemplate(self::QUESTION_HAVE_EDUCATION)
            ],
            self::QUESTION_WORK_EXPERIENCE => [
                'answers' => $mifidQuestionnaire->getWorkExperience(),
                //'label' => self::includeMifidTemplate(self::QUESTION_WORK_EXPERIENCE)
            ],
            self::QUESTION_PLATFORM_EXPERIENCE => [
                'answers' => $mifidQuestionnaire->getPlatformExperience(),
                //'label' => self::includeMifidTemplate(self::QUESTION_PLATFORM_EXPERIENCE)
            ],
            self::QUESTION_ORGANIZED_MARKET_TRADES => [
                'answers' => $mifidQuestionnaire->getOrganizedMarketTrades(),
                //'label' => self::includeMifidTemplate(self::QUESTION_PLATFORM_EXPERIENCE)
            ],
            self::QUESTION_LEVERAGE_MARKET_TRADES => [
                'answers' => $mifidQuestionnaire->getLeveragedMarketTrades(),
                //'label' => self::includeMifidTemplate(self::QUESTION_LEVERAGE_MARKET_TRADER),
            ],
            self::QUESTION_AVERAGE_TRANSACTION_VALUE => [
                'answers' => $mifidQuestionnaire->getAverageTransactionValue(),
                //'label' => self::includeMifidTemplate(self::QUESTION_AVERAGE_TRANSACTION_VALUE),
            ],
            self::QUESTION_NATURE_OF_INVESTMENT => [
                'answers' => $mifidQuestionnaire->getNatureOfInvestment(),
                //'label' => self::includeMifidTemplate(self::QUESTION_NATURE_OF_INVESTMENT),
            ],
            self::QUESTION_ORDER_TYPE_QUESTION => [
                'answers' => $mifidQuestionnaire->getOrderTypeQuestion(),
                //'label' => self::includeMifidTemplate(self::QUESTION_ORDER_TYPE_QUESTION),
            ],
            self::QUESTION_INVEST_RESULT_QUESTION => [
                'answers' => $mifidQuestionnaire->getInvestmentResultQuestion(),
                //'label' => self::includeMifidTemplate(self::QUESTION_INVEST_RESULT_QUESTION)
                ],
            self::QUESTION_HAVE_LOSS_AWARENESS => [
                'answers' => ['YES','NO'],
                //'label' => self::includeMifidTemplate(self::QUESTION_HAVE_LOSS_AWARENESS)
            ],
            self::QUESTION_PURPOSE_OF_INVESTMENT => [
                'answers' => $mifidQuestionnaire->getPurposeOfInvestment(),
                //'label' => self::includeMifidTemplate(self::QUESTION_PURPOSE_OF_INVESTMENT)
                ],
            self::QUESTION_INVEST_PERCENTAGE_OF_SAVING => [
                'answers' => $mifidQuestionnaire->getInvestPercentOfSavings(),
                //'label' => self::includeMifidTemplate(self::QUESTION_INVEST_PERCENTAGE_OF_SAVING)
            ],
            self::QUESTION_ACCEPT_RISK_OF_LOSS => [
                'answers' => $mifidQuestionnaire->getAcceptRiskOfLoss(),
                //'label' => self::includeMifidTemplate(self::QUESTION_ACCEPT_RISK_OF_LOSS)
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getAgreementsForFirstStep()
    {
        return [
            0 => [
                'label' => 'Wyrażam zgodę na otrzymywanie od Domu Maklerskiego TMS Brokers S.A. newsletterów, smsów, mmsów oraz informacji marketingowych na temat usług świadczonych przez tę spółkę oraz o samej spółce, w tym o promocjach, wydarzeniach promocyjnych i innych akcjach marketingowych. W każdej chwili mogę wycofać tę zgodę.',
                'link' => null,
            ],
            1 => [
                'label' => 'Wyrażam zgodę na przekazywanie mi informacji marketingowych przez Dom Maklerski TMS Brokers S.A. na temat usług świadczonych przez tę spółkę oraz o samej spółce, w tym o promocjach, wydarzeniach promocyjnych i innych akcjach marketingowych w trakcie połączeń głosowych wykonywanych na mój numer telefonu. W każdej chwili mogę wycofać tę zgodę. ',
                'link' => null,
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getAgreementsForSecondStep()
    {
        return [
            0 => [
                'label' => 'Regulamin zawierania umów drogą elektroniczną',
                'link' => 'tms.pl/document/315809',
            ],
            1 => [
                'label' => 'Umowa elektroniczna',
                'link' => 'tms.pl/document/315810',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getAgreementsForThirdStep()
    {
        return [
            0 => [
                'label' => Yii::t('app', 'Information Card'),
                'link' => 'tms.pl/document/144425',
            ],
            1 => [
                'label' => Yii::t('app', 'Terms and Regulations'),
                'link' => 'tms.pl/document/330332',
            ],
            2 => [
                'label' => Yii::t('app', 'Framework Agreement'),
                'link' => 'tms.pl/document/144427',
            ],
            3 => [
                'label' => Yii::t('app', 'Financial Instrument Characteristic'),
                'link' => 'tms.pl/document/116240',
            ],
            4 => [
                'label' => Yii::t('app', 'Table of Fees and Commissions'),
                'link' => 'tms.pl/document/116256',
            ],
            5 => [
                'label' => Yii::t('app', 'Financial Instruments Specification'),
                'link' => 'tms.pl/document/116271',
            ],
            6 => [
                'label' => Yii::t('app', 'Order execution policy'),
                'link' => 'tms.pl/document/313092',
            ],
            7 => [
                'label' => Yii::t('app', 'Tabela punktów swapowych'),
                'link' => 'tms.pl/document/144951',
            ],
        ];
    }
}