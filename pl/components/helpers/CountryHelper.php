<?php

namespace app\modules\pl\components\helpers;

use Yii;

class CountryHelper
{
    /**
     * @return array
     */
    public static function getList()
    {
        return [
            'PL' => Yii::t('app', 'Polska'),
            'AF' => Yii::t('app', 'Afghanistan'),
            'AL' => Yii::t('app', 'Albania'),
            'DZ' => Yii::t('app', 'Algieria'),
            'AD' => Yii::t('app', 'Andora'),
            'AO' => Yii::t('app', 'Angola'),
            'AI' => Yii::t('app', 'Anguilla'),
            'AG' => Yii::t('app', 'Antigua i Barbuda'),
            'AN' => Yii::t('app', 'Antyle Holenderskie'),
            'SA' => Yii::t('app', 'Arabia Saudyjska'),
            'AR' => Yii::t('app', 'Argentyna'),
            'AM' => Yii::t('app', 'Armenia'),
            'AW' => Yii::t('app', 'Aruba'),
            'AU' => Yii::t('app', 'Australia'),
            'AT' => Yii::t('app', 'Austria'),
            'AZ' => Yii::t('app', 'Azerbejdżan'),
            'BS' => Yii::t('app', 'Bahamy'),
            'BH' => Yii::t('app', 'Bahrajn'),
            'BD' => Yii::t('app', 'Bangladesz'),
            'BB' => Yii::t('app', 'Barbados'),
            'BE' => Yii::t('app', 'Belgia'),
            'BZ' => Yii::t('app', 'Belize'),
            'BM' => Yii::t('app', 'Bermuda'),
            'BT' => Yii::t('app', 'Bhutan'),
            'BY' => Yii::t('app', 'Białoruś'),
            'BO' => Yii::t('app', 'Boliwia'),
            'BA' => Yii::t('app', 'Bośnia i Hercegowina'),
            'BW' => Yii::t('app', 'Botswana'),
            'BR' => Yii::t('app', 'Brazylia'),
            'BN' => Yii::t('app', 'Brunei'),
            'IO' => Yii::t('app', 'Brytyjskie Terytorium Oceanu Indyjskiego'),
            'BG' => Yii::t('app', 'Bułgaria'),
            'BF' => Yii::t('app', 'Burkina Faso'),
            'BI' => Yii::t('app', 'Burundi'),
            'CL' => Yii::t('app', 'Chile'),
            'CN' => Yii::t('app', 'Chiny'),
            'HR' => Yii::t('app', 'Chorwacja'),
            'CY' => Yii::t('app', 'Cypr'),
            'TD' => Yii::t('app', 'Czad'),
            'CG' => Yii::t('app', 'Czarnogóra'),
            'CZ' => Yii::t('app', 'Czechy'),
            'DK' => Yii::t('app', 'Dania'),
            'DM' => Yii::t('app', 'Dominika'),
            'DO' => Yii::t('app', 'Dominikana'),
            'DJ' => Yii::t('app', 'Dżibuti'),
            'EC' => Yii::t('app', 'Ekwador'),
            'EG' => Yii::t('app', 'Egipt'),
            'ER' => Yii::t('app', 'Erytrea'),
            'EE' => Yii::t('app', 'Estonia'),
            'FK' => Yii::t('app', 'Falklandy'),
            'FJ' => Yii::t('app', 'Fidżi'),
            'PH' => Yii::t('app', 'Filipiny'),
            'FI' => Yii::t('app', 'Finlandia'),
            'FR' => Yii::t('app', 'Francja'),
            'GA' => Yii::t('app', 'Gabon'),
            'GM' => Yii::t('app', 'Gambia'),
            'GS' => Yii::t('app', 'Georgia Południowa i Sandwich Południowy'),
            'GH' => Yii::t('app', 'Ghana'),
            'GI' => Yii::t('app', 'Gibraltar'),
            'GR' => Yii::t('app', 'Grecja'),
            'GL' => Yii::t('app', 'Greenlandia'),
            'GD' => Yii::t('app', 'Grenada'),
            'GE' => Yii::t('app', 'Gruzja'),
            'GU' => Yii::t('app', 'Guam'),
            'GBG' => Yii::t('app', 'Guernsey'),
            'GW' => Yii::t('app', 'Guinea-Bissau'),
            'GF' => Yii::t('app', 'Gujana Francuska'),
            'GY' => Yii::t('app', 'Guyana'),
            'GP' => Yii::t('app', 'Gwadelupa'),
            'GT' => Yii::t('app', 'Gwatemala'),
            'GN' => Yii::t('app', 'Gwinea'),
            'GQ' => Yii::t('app', 'Gwinea Równikowa'),
            'HT' => Yii::t('app', 'Haiti'),
            'ES' => Yii::t('app', 'Hiszpania'),
            'NL' => Yii::t('app', 'Holandia'),
            'HN' => Yii::t('app', 'Honduras'),
            'HK' => Yii::t('app', 'Hongkong'),
            'HU' => Yii::t('app', 'Hungary'),
            'IN' => Yii::t('app', 'Indie'),
            'ID' => Yii::t('app', 'Indonezja'),
            'IR' => Yii::t('app', 'Iran'),
            'IQ' => Yii::t('app', 'Irak'),
            'IE' => Yii::t('app', 'Irlandia'),
            'IS' => Yii::t('app', 'Islandia'),
            'IL' => Yii::t('app', 'Izrael'),
            'JM' => Yii::t('app', 'Jamajka'),
            'JP' => Yii::t('app', 'Japonia'),
            'YE' => Yii::t('app', 'Jemen'),
            'GBJ' => Yii::t('app', 'Jersey'),
            'JO' => Yii::t('app', 'Jordania'),
            'KY' => Yii::t('app', 'Kajmany'),
            'KH' => Yii::t('app', 'Kambodża'),
            'CM' => Yii::t('app', 'Kamerun'),
            'CA' => Yii::t('app', 'Kanada'),
            'QA' => Yii::t('app', 'Katar'),
            'KZ' => Yii::t('app', 'Kazachstan'),
            'KE' => Yii::t('app', 'Kenia'),
            'KI' => Yii::t('app', 'Kiribati'),
            'CO' => Yii::t('app', 'Kolumbia'),
            'KM' => Yii::t('app', 'Komory'),
            'CD' => Yii::t('app', 'Kongo'),
            'KP' => Yii::t('app', 'Korea Północna'),
            'KR' => Yii::t('app', 'Korea Południowa'),
            'CR' => Yii::t('app', 'Kostaryka'),
            'CU' => Yii::t('app', 'Kuba'),
            'KW' => Yii::t('app', 'Kuwejt'),
            'KG' => Yii::t('app', 'Kirgistan'),
            'LA' => Yii::t('app', 'Laos'),
            'LV' => Yii::t('app', 'Łotwa'),
            'LB' => Yii::t('app', 'Liban'),
            'LS' => Yii::t('app', 'Lesotho'),
            'LR' => Yii::t('app', 'Liberia'),
            'LY' => Yii::t('app', 'Libia'),
            'LI' => Yii::t('app', 'Liechtenstein'),
            'LT' => Yii::t('app', 'Lithwa'),
            'LU' => Yii::t('app', 'Luksemburg'),
            'MO' => Yii::t('app', 'Makau'),
            'MK' => Yii::t('app', 'Macedonia'),
            'MG' => Yii::t('app', 'Madagaskar'),
            'MW' => Yii::t('app', 'Malawi'),
            'MV' => Yii::t('app', 'Malediwy'),
            'MY' => Yii::t('app', 'Malezja'),
            'ML' => Yii::t('app', 'Mali'),
            'MT' => Yii::t('app', 'Malta'),
            'MP' => Yii::t('app', 'Mariany Północne'),
            'MQ' => Yii::t('app', 'Martynika'),
            'MR' => Yii::t('app', 'Mauretania'),
            'MU' => Yii::t('app', 'Mauritius'),
            'YT' => Yii::t('app', 'Majotta'),
            'MX' => Yii::t('app', 'Meksyk'),
            'FM' => Yii::t('app', 'Mikronezja'),
            'MD' => Yii::t('app', 'Mołdawia'),
            'MC' => Yii::t('app', 'Monako'),
            'MN' => Yii::t('app', 'Mongolia'),
            'MS' => Yii::t('app', 'Montserrat'),
            'MA' => Yii::t('app', 'Maroko'),
            'MZ' => Yii::t('app', 'Mozambik'),
            'MM' => Yii::t('app', 'Myanmar (Birma)'),
            'NA' => Yii::t('app', 'Namibia'),
            'NR' => Yii::t('app', 'Nauru'),
            'NP' => Yii::t('app', 'Nepal'),
            'NZ' => Yii::t('app', 'Nowa Zelandia'),
            'DE' => Yii::t('app', 'Niemcy'),
            'NE' => Yii::t('app', 'Niger'),
            'NG' => Yii::t('app', 'Nigeria'),
            'NI' => Yii::t('app', 'Nikaragua'),
            'UN' => Yii::t('app', 'Niue'),
            'NF' => Yii::t('app', 'Norfolk'),
            'NO' => Yii::t('app', 'Norwegia'),
            'NC' => Yii::t('app', 'Nowa Kaledonia'),
            'OM' => Yii::t('app', 'Oman'),
            'PK' => Yii::t('app', 'Pakistan'),
            'PW' => Yii::t('app', 'Palau'),
            'PA' => Yii::t('app', 'Panama'),
            'PG' => Yii::t('app', 'Papua-Nowa Gwinea'),
            'PY' => Yii::t('app', 'Paragwaj'),
            'PE' => Yii::t('app', 'Peru'),
            'PR' => Yii::t('app', 'Portoryko'),
            'PT' => Yii::t('app', 'Portugalia'),
            'CF' => Yii::t('app', 'Republika Środkowoafrykańska'),
            'CV' => Yii::t('app', 'Republika Zielonego Przylądka'),
            'RE' => Yii::t('app', 'Reunion'),
            'RO' => Yii::t('app', 'Rumunia'),
            'RU' => Yii::t('app', 'Rosja'),
            'ZA' => Yii::t('app', 'RPA'),
            'RW' => Yii::t('app', 'Rwanda'),
            'EH' => Yii::t('app', 'Sahara Zachodnia'),
            'KN' => Yii::t('app', 'Saint Kitts i Nevis'),
            'LC' => Yii::t('app', 'Saint Lucia'),
            'PM' => Yii::t('app', 'Saint-Pierre i Miquelon'),
            'VC' => Yii::t('app', 'Saint Vincent i Grenadyny'),
            'SV' => Yii::t('app', 'Salvador'),
            'WS' => Yii::t('app', 'Samoa'),
            'AS' => Yii::t('app', 'Samoa Amerykańskie'),
            'SM' => Yii::t('app', 'San Marino'),
            'SN' => Yii::t('app', 'Senegal'),
            'YU' => Yii::t('app', 'Serbia'),
            'SC' => Yii::t('app', 'Seszele'),
            'SL' => Yii::t('app', 'Sierra Leone'),
            'SG' => Yii::t('app', 'Singapur'),
            'SK' => Yii::t('app', 'Słowacja'),
            'SI' => Yii::t('app', 'Słowenia'),
            'SO' => Yii::t('app', 'Somalia'),
            'LK' => Yii::t('app', 'Sri Lanka'),
            'VA' => Yii::t('app', 'State of the Vatican City'),
            'EU' => Yii::t('app', 'Strefa Euro'),
            'SD' => Yii::t('app', 'Sudan'),
            'DR' => Yii::t('app', 'Surinam'),
            'SE' => Yii::t('app', 'Szwecja'),
            'SH' => Yii::t('app', 'Święta Helena'),
            'CH' => Yii::t('app', 'Szwajcaria'),
            'SY' => Yii::t('app', 'Syria'),
            'TJ' => Yii::t('app', 'Tadżykistan'),
            'TH' => Yii::t('app', 'Tajlandia'),
            'TW' => Yii::t('app', 'Tajwan'),
            'TZ' => Yii::t('app', 'Tanzania'),
            'TP' => Yii::t('app', 'Timor Zachodni'),
            'TG' => Yii::t('app', 'Togo'),
            'TK' => Yii::t('app', 'Tokelau'),
            'TO' => Yii::t('app', 'Tonga'),
            'TN' => Yii::t('app', 'Tunezja'),
            'TR' => Yii::t('app', 'Turcja'),
            'TM' => Yii::t('app', 'Turkmenistan'),
            'TC' => Yii::t('app', 'Turks i Caicos'),
            'TV' => Yii::t('app', 'Tuvalu'),
            'TT' => Yii::t('app', 'Trynidad i Tobago'),
            'UG' => Yii::t('app', 'Uganda'),
            'UA' => Yii::t('app', 'Ukraina'),
            'UY' => Yii::t('app', 'Urugwaj'),
            'UZ' => Yii::t('app', 'Uzbekistan'),
            'US' => Yii::t('app', 'USA'),
            'VU' => Yii::t('app', 'Vanuatu'),
            'VE' => Yii::t('app', 'Venezuela'),
            'VN' => Yii::t('app', 'Wietnam'),
            'WF' => Yii::t('app', 'Wallis i Futuna'),
            'GB' => Yii::t('app', 'Wielka Brytania'),
            'IT' => Yii::t('app', 'Włochy'),
            'CI' => Yii::t('app', 'Wybrzeże Kości Słoniowej'),
            'AX' => Yii::t('app', 'Wyspy Alandzkie'),
            'BV' => Yii::t('app', 'Wyspa Bouveta'),
            'CX' => Yii::t('app', 'Wyspa Bożego Narodzenia'),
            'CK' => Yii::t('app', 'Wyspy Cooka'),
            'VG' => Yii::t('app', 'Wyspy Dziewicze (Brytyjskie)'),
            'VI' => Yii::t('app', 'Wyspy Dziewicze (Stanów Zjednoczonych)'),
            'CC' => Yii::t('app', 'Wyspy Kokosowe'),
            'GBM' => Yii::t('app', 'Wyspa Man'),
            'MH' => Yii::t('app', 'Wyspy Marshalla'),
            'FO' => Yii::t('app', 'Wyspy Owcze'),
            'PN' => Yii::t('app', 'Wyspy Pitcairn'),
            'SB' => Yii::t('app', 'Wyspy Salomona'),
            'ST' => Yii::t('app', 'Wyspy Świętego Tomasza i Książęca'),
            'ZRCD' => Yii::t('app', 'Zair'),
            'ZM' => Yii::t('app', 'Zambia'),
            'ZW' => Yii::t('app', 'Zimbabwe'),
            'AE' => Yii::t('app', 'Zjednoczone Emiraty Arabskie'),
        ];
    }

    /**
     * @return array
     */
    public static function getKeysList()
    {
        return array_keys(self::getList());
    }

    /**
     * @return array
     */
    public static function getEuropeanUnionCountryList()
    {
        return [
            'AT' => 'austriackie',
            'BE' => 'belgijskie',
            'BG' => 'bułgarskie',
            'CZ' => 'czeskie',
            'DK' => 'duńskie',
            'EE' => 'estońskie',
            'ES' => 'hiszpańskie',
            'FI' => 'fińskie',
            'GB' => 'brytyjskie',
            'GR' => 'greckie',
            'HR' => 'chorwackie',
            'IS' => 'islandzkie',
            'IT' => 'włoskie',
            'LT' => 'litewskie',
            'LV' => 'łotewskie',
            'PL' => 'polskie',
            'DE' => 'niemieckie',
            'FR' => 'francuskie',
            'HU' => 'węgierskie',
            'IE' => 'irlandzkie',
            'LU' => 'luksemburskie',
            'MT' => 'maltańskie',
            'NO' => 'norweskie',
            'PT' => 'portugalskie',
            'RO' => 'rumuńskie',
            'SE' => 'szwedzkie',
            'SI' => 'słoweńskie',
            'SK' => 'słowackie',
        ];
    }

    /**
     * @return array
     */
    public static function getCitizenshipList()
    {
        return [
            'PL' => 'polskie',
            'AF' => 'afghańskie',
            'AL' => 'albańskie',
            'DZ' => 'algierskie',
            'AD' => 'andorskie',
            'AO' => 'angolskie',
            'AI' => 'anguillskie',
            'AG' => 'antigua i barbuda',
            'AN' => 'antylskie',
            'SA' => 'saudyjskie',
            'AR' => 'argentyńskie',
            'AM' => 'armeńskie',
            'AW' => 'arubskie',
            'AU' => 'australijskie',
            'AT' => 'austriackie',
            'AZ' => 'azerbejdżańskie',
            'BS' => 'bahamskie',
            'BH' => 'bahrajńskie',
            'BD' => 'bangladeskie',
            'BB' => 'barbadoskie',
            'BE' => 'belgijskie',
            'BZ' => 'beliskie',
            'BM' => 'bermudzkie',
            'BT' => 'bhutańskie',
            'BY' => 'białoruskie',
            'BO' => 'boliwijskie',
            'BA' => 'bośniackie',
            'BW' => 'botswańskie',
            'BR' => 'brazylijskie',
            'BN' => 'brunejskie',
            'IO' => 'brytyjskie terytorium oceanu indyjskiego',
            'BG' => 'bułgarskie',
            'BF' => 'burkińskie',
            'BI' => 'burundzkie',
            'CL' => 'chilijskie',
            'CN' => 'chińskie',
            'HR' => 'chorwackie',
            'CY' => 'cypryjskie',
            'TD' => 'czadzkie',
            'CG' => 'czarnogórskie',
            'CZ' => 'czeskie',
            'DK' => 'duńskie',
            'DM' => 'dominickie',
            'DO' => 'dominikańskie',
            'DJ' => 'dżibuckie',
            'EC' => 'ekwadorskie',
            'EG' => 'egipckie',
            'ER' => 'erytrejskie',
            'EE' => 'estońskie',
            'ET' => 'etiopskie',
            'FK' => 'falklandzkie',
            'FJ' => 'fidżijskie',
            'PH' => 'filipińskie',
            'FI' => 'finlandzkie',
            'FR' => 'francuskie',
            'GA' => 'gabońskie',
            'GM' => 'gambijskie',
            'GS' => 'georgijskie południowa i sandwich południowy',
            'GH' => 'ghańskie',
            'GI' => 'gibraltarskie',
            'GR' => 'greckie',
            'GL' => 'greenlandzkie',
            'GD' => 'grenadzkie',
            'GE' => 'gruzińskie',
            'GU' => 'guamskie',
            'GBG' => 'guernsey',
            'GW' => 'guinea-bissau',
            'GF' => 'gujana francuska',
            'GY' => 'guyańskie',
            'GP' => 'gwadelupskie',
            'GT' => 'gwatemalskie',
            'GN' => 'gwinejskie',
            'GQ' => 'gwinea równikowa',
            'HT' => 'haitańskie',
            'ES' => 'hiszpanskie',
            'NL' => 'holenderskie',
            'HN' => 'honduraskie',
            'HK' => 'hongkong',
            'HU' => 'węgierskie',
            'IN' => 'indyjskie',
            'ID' => 'indonezyjskie',
            'IR' => 'irańskie',
            'IQ' => 'irackie',
            'IE' => 'irlandzkie',
            'IS' => 'islandzkie',
            'IL' => 'izraelskie',
            'JM' => 'jamajskie',
            'JP' => 'japońskie',
            'YE' => 'jemenskie',
            'GBJ' => 'jersey',
            'JO' => 'jordańskie',
            'KY' => 'kajmańskie',
            'KH' => 'kambodżańskie',
            'CM' => 'kameruńskie',
            'CA' => 'kanadyjskie',
            'QA' => 'katarskie',
            'KZ' => 'kazachstańskie',
            'KE' => 'kenijskie',
            'KI' => 'kiribackie',
            'CO' => 'kolumbijskie',
            'KM' => 'komorskie',
            'CD' => 'kongijskie',
            'KP' => 'północnokoreańskie',
            'KR' => 'południowokoreańskie',
            'CR' => 'kostaryckie',
            'CU' => 'kubańskie',
            'KW' => 'kuwejckie',
            'KG' => 'kirgistańskie',
            'LA' => 'laostańskie',
            'LV' => 'Łotewskie',
            'LB' => 'libańskie',
            'LS' => 'lesotho',
            'LR' => 'liberijskie',
            'LY' => 'libijskie',
            'LI' => 'liechtenstein',
            'LT' => 'litewskie',
            'LU' => 'luksemburskie',
            'MO' => 'makau',
            'MK' => 'macedońskie',
            'MG' => 'madagaskarskie',
            'MW' => 'malawijskie',
            'MV' => 'malediwskie',
            'MY' => 'malezyjskie',
            'ML' => 'malijskie',
            'MT' => 'maltańskie',
            'MP' => 'mariany północne',
            'MQ' => 'martynika',
            'MR' => 'mauretańskie',
            'MU' => 'mauritiuskie',
            'YT' => 'majottańskie',
            'MX' => 'meksykańskie',
            'FM' => 'mikronezyjskie',
            'MD' => 'mołdawskie',
            'MC' => 'monako',
            'MN' => 'mongolskie',
            'MS' => 'montserrackie',
            'MA' => 'marokańskie',
            'MZ' => 'mozambijskie',
            'MM' => 'birmańskie',
            'NA' => 'namibijskie',
            'NR' => 'nauru',
            'NP' => 'nepalskie',
            'NZ' => 'nowozelandzkie',
            'DE' => 'niemieckie',
            'NE' => 'nigerskie',
            'NG' => 'nigeryjskie',
            'NI' => 'nikaraguańskie',
            'UN' => 'niue',
            'NF' => 'norfolckie',
            'NO' => 'norweskie',
            'NC' => 'nowolaledońskie',
            'OM' => 'omańskie',
            'PK' => 'pakistańskie',
            'PW' => 'palauskie',
            'PA' => 'panamskie',
            'PG' => 'papuaskie',
            'PY' => 'paragwajskie',
            'PE' => 'peruwiańskie',
            'PR' => 'portorykańskie',
            'PT' => 'portugalskie',
            'CF' => 'republika Środkowoafrykańska',
            'CV' => 'republika zielonego przylądka',
            'RE' => 'reunion',
            'RO' => 'rumuńskie',
            'RU' => 'rosysjkie',
            'ZA' => 'rpa',
            'RW' => 'rwandyjskie',
            'EH' => 'sahara zachodnia',
            'KN' => 'saint kitts i nevis',
            'LC' => 'saint lucia',
            'PM' => 'saint-pierre i miquelon',
            'VC' => 'saint vincent i grenadyny',
            'SV' => 'salwadorskie',
            'WS' => 'samoańskie',
            'AS' => 'samoa amerykańskie',
            'SM' => 'san marino',
            'SN' => 'senegalskie',
            'YU' => 'serbskie',
            'SC' => 'seszeleskie',
            'SL' => 'sierra leone',
            'SG' => 'singapurskie',
            'SK' => 'słowackie',
            'SI' => 'słoweńskie',
            'SO' => 'somalijskie',
            'LK' => 'lankijskie',
            'VA' => 'state of the vatican city',
            'EU' => 'strefa euro',
            'SD' => 'sudańskie',
            'DR' => 'surinamskie',
            'SE' => 'szwedzkie',
            'SH' => 'Święta helena',
            'CH' => 'szwajcarskie',
            'SY' => 'syryjskie',
            'TJ' => 'tadżykistańskie',
            'TH' => 'tajlandzkie',
            'TW' => 'tajwańskie',
            'TZ' => 'tanzańskie',
            'TP' => 'timor zachodni',
            'TG' => 'togo',
            'TK' => 'tokelau',
            'TO' => 'tongijskie',
            'TN' => 'tunezyjskie',
            'TR' => 'tureckie',
            'TM' => 'turkmenistańskie',
            'TC' => 'turks i caicos',
            'TV' => 'tuvalu',
            'TT' => 'trynidad i tobago',
            'UG' => 'uganda',
            'UA' => 'ukraińskie',
            'UY' => 'urugwajskie',
            'UZ' => 'uzbekistańskie',
            'US' => 'amerykańskie',
            'VU' => 'vanuatu',
            'VE' => 'venezuelskie',
            'VN' => 'wietnamskie',
            'WF' => 'wallis i futuna',
            'GB' => 'brytyjskie',
            'IT' => 'włoskie',
            'CI' => 'wybrzeże kości słoniowej',
            'AX' => 'wyspy alandzkie',
            'BV' => 'wyspa bouveta',
            'CX' => 'wyspa bożego narodzenia',
            'CK' => 'wyspy cooka',
            'VG' => 'wyspy dziewicze (brytyjskie)',
            'VI' => 'wyspy dziewicze (stanów zjednoczonych)',
            'CC' => 'wyspy kokosowe',
            'GBM' => 'wyspa man',
            'MH' => 'wyspy marshalla',
            'FO' => 'wyspy owcze',
            'PN' => 'wyspy pitcairn',
            'SB' => 'wyspy salomona',
            'ST' => 'wyspy Świętego tomasza i książęca',
            'ZRCD' => 'zairskie',
            'ZM' => 'zambijskie',
            'ZW' => 'zimbabwe',
            'AE' => 'zjednoczone emiraty arabskie'
        ];
    }

    /**
     * @param null|string $labelKey
     * @return string|null
     */
    public static function getNationalIdentificationNumberLabel($labelKey = null)
    {
        $countries = [
            'BE' => 'Belgijski numer krajowy (Numéro de registre national – Rijksregisternummer)"',
            'BG' => 'Bułgarski osobisty numer identyfikacyjny',
            'CZ' => 'Krajowy numer identyfikacyjny',
            'DK' => 'Osobisty kod identyfikacyjny',
            'EE' => 'Estoński osobisty kod identyfikacyjny (Isikukood)',
            'ES' => 'Numer identyfikacji podatkowej (Código de identificación fiscal)',
            'FI' => 'Osobisty kod identyfikacyjny',
            'GB' => 'Krajowy numer ubezpieczenia Zjednoczonego Królestwa',
            'GR' => '10-cyfrowy numer w systemie DSS',
            'HR' => 'Osobisty numer identyfikacyjny (OIB – Osobni identifikacijski broj)',
            'IS' => 'Osobisty kod identyfikacyjny (Kennitala)',
            'IT' => 'Numer identyfikacji podatkowej (Codice fiscale)',
            'LT' => 'Numer osobisty (Asmens kodas)',
            'LV' => 'Numer osobisty (Personas kods)',
            'MT' => 'Krajowy numer identyfikacyjny',
            'NO' => '11-cyfrowy osobisty numer identyfikacyjny (Foedselsnummer)',
            'PT' => 'Numer identyfikacji podatkowej (Número de Identificação Fiscal)',
            'RO' => 'Krajowy numer identyfikacyjny (Cod Numeric Personal)',
            'SE' => 'Osobisty numer tożsamości',
            'SI' => 'Osobisty numer identyfikacyjny (EMŠO: Enotna Matična Številka Občana)',
            'SK' => 'Osobisty numer identyfikacyjny (Rodné číslo)',
        ];

        return $labelKey && isset($countries[$labelKey]) ? $countries[$labelKey] : null;
    }
}