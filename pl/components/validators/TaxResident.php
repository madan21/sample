<?php

namespace app\modules\pl\components\validators;

use app\modules\pl\models\RealForm;
use yii\validators\Validator;
use app\modules\pl\components\helpers\RealFormHelper;

/**
 * Class TaxResident
 * @package app\modules\pl\components\validators
 *
 * @property string $poland_tax_office_id
 * @property string $other_country_tax_resident_value
 * @property string $tin
 * @property string $not_have_tin
 * @property string $not_have_tin_reason
 * @property string $usa_tin
 *
 * @property array $taxResidentValue
 * @property boolean $isPolandTaxResident
 * @property boolean $isOtherCountryTaxResident
 * @property boolean $isUsaTaxResident
 */
class TaxResident extends Validator
{
    public $poland_tax_office_id;
    public $other_country_tax_resident_value;
    public $tin;
    public $not_have_tin;
    public $not_have_tin_reason;
    public $usa_tin;
    public $taxResidentValue = [];

    /**
     * @param RealForm $model
     * @param string $attribute
     * @return  boolean
     */
    public function validateAttribute($model, $attribute)
    {
        $this->taxResidentValue = $model->$attribute;

        if(empty($this->taxResidentValue)) {
            $this->addError($model, $attribute, 'Musisz wybrać rezydencję podatkową');
            return false;
        }

        /**
         * @var integer $polandTaxOfficeId
         * @var string $tinValue
         * @var boolean $notHaveTinValue
         * @var string $notHaveTinReasonValue
         * @var string $usaTinValue
         */
        $polandTaxOfficeId = $model->{$this->poland_tax_office_id};
        $tinValue = $model->{$this->tin};
        $notHaveTinValue = (empty($model->{$this->not_have_tin}) || $model->{$this->not_have_tin} == false) ? false : true;
        $notHaveTinReasonValue = $model->{$this->not_have_tin_reason};
        $usaTinValue = $model->{$this->usa_tin};

        if($this->isPolandTaxResident) {
            if(empty($polandTaxOfficeId)) {
                $this->addError($model, $this->poland_tax_office_id, 'Wybierz z listy urząd podatkowy');
            }
        } elseif($polandTaxOfficeId != RealForm::DEFAULT_POLAND_TAX_OFFICE_ID) { //for non residence we are not showing tax_office but in javascript its already make selected
            $this->addError($model, $this->poland_tax_office_id, 'Dla Nierezydenta jest dostępny "Warszawa - ul. Lindleya 14, 02-013"');
        }

        if($this->isOtherCountryTaxResident) {

            /* @var string $otherCountryTaxResidentValue */
            $otherCountryTaxResidentValue = $model->{$this->other_country_tax_resident_value};

            if(empty($otherCountryTaxResidentValue)) {
                $this->addError($model, $this->other_country_tax_resident_value, 'Wymagane jest informacje o opodatkowaniu w innym kraju');
            }

            if(empty($tinValue) && !$notHaveTinValue) {
                $this->addError($model, $this->tin, 'Niewłaściwy numer identyfikacji podatkowej');
            }

            if(empty($notHaveTinReasonValue) && $notHaveTinValue) {
                $this->addError($model, $this->not_have_tin_reason, 'Wyjaśnić w przypadku braku TIN  wymagane');
            }
        }

        if($this->isUsaTaxResident) {
            if(empty($usaTinValue)) {
                $this->addError($model, $this->usa_tin, 'Proszę podać numer TIN');
            }
        }
    }

    /**
     * @return bool
     */
    protected function getIsPolandTaxResident()
    {
        return in_array(RealFormHelper::TAX_RESIDENT_POLAND, $this->taxResidentValue);
    }

    /**
     * @return bool
     */
    protected function getIsOtherCountryTaxResident()
    {
        return in_array(RealFormHelper::TAX_RESIDENT_OTHER_COUNTRY, $this->taxResidentValue);
    }

    /**
     * @return bool
     */
    protected function getIsUsaTaxResident()
    {
        return in_array(RealFormHelper::TAX_RESIDENT_USA, $this->taxResidentValue);
    }
}