<?php

namespace app\modules\pl\components\validators;

use app\modules\pl\components\helpers\RealFormHelper;
use yii\validators\Validator;
use app\modules\pl\models\RealForm;
use app\modules\pl\components\helpers\CountryHelper;

/**
 * Class PersonIdDocumentType
 * @package app\modules\pl\components\validators
 *
 * @property string $citizenship
 */
class PersonIdDocumentType extends Validator
{
    public $citizenship;

    /**
     * @param RealForm $model
     * @param string $attribute
     * @return boolean
     */
    public function validateAttribute($model, $attribute)
    {
        if(!array_key_exists($model->{$this->citizenship}, CountryHelper::getEuropeanUnionCountryList()) && ($model->$attribute != RealFormHelper::PASSPORT_PERSONAL_DOCUMENT)) {
            $this->addError($model, $attribute, 'Dla podanego obywatelwstwa akceptujemy tylko paszport. Proszę zaznaczyć z pola wyboru opcję "Paszport"');
            return false;
        }

        return true;
    }
}