<?php

namespace app\modules\pl\components\validators;

use app\modules\pl\components\helpers\CountryHelper;
use yii\validators\Validator;
use app\modules\pl\models\RealForm;

/**
 * Class Citizenship
 * @package app\modules\pl\components\validators
 *
 * @property string $national_identification_number
 */
class Citizenship extends Validator
{
    public $national_identification_number;

    /**
     * @param RealForm $model
     * @param string $attribute
     * @return boolean
     */
    public function validateAttribute($model, $attribute)
    {
        $nationalIdentificationCountry = CountryHelper::getNationalIdentificationNumberLabel($model->$attribute);

        if(!$this->requiredNationalIdentificationNumberValidation($nationalIdentificationCountry)) {
            $this->addError($model, $attribute, 'Wprowadź poprawny “Krajowy numer identyfikacyjny”');
            return false;
        }

        if(!$this->formatNationalIdentificationNumberValidation($nationalIdentificationCountry)) {
            $this->addError($model, 'person_id_document_number', 'Podany numer dokumentu tożsamości jest niepoprawny');
            return false;
        }

        return true;
    }

    /**
     * @param string $nationalIdentificationCountryLabel
     * @return bool
     */
    protected function requiredNationalIdentificationNumberValidation($nationalIdentificationCountryLabel)
    {
        if(!is_null($nationalIdentificationCountryLabel) && empty($this->national_identification_number)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $nationalIdentificationCountryLabel
     * @return bool
     */
    protected function formatNationalIdentificationNumberValidation($nationalIdentificationCountryLabel)
    {
        if(!is_null($nationalIdentificationCountryLabel) && !preg_match(RealForm::NATIONAL_ID_NUMBER_PATTERN, $nationalIdentificationCountryLabel)) {
            return false;
        }

        return true;
    }
}