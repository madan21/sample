<?php

namespace app\modules\pl\components\validators;

use app\modules\pl\components\helpers\RealFormHelper;
use yii\validators\Validator;
use app\modules\pl\models\RealForm;
use Tms\Models\Ws\MifidQuestionnaire;

/**
 * Class FirstQuest
 * @package app\modules\pl\components\validators
 *
 */
class FirstQuest extends Validator
{
    /**
     * @param RealForm $model
     * @param string $attribute
     * @return boolean
     */
    public function validateAttribute($model, $attribute)
    {
        /* @var array $allAnswers */
        $allAnswers = $model->$attribute;
        $mifidQuestionStructure = RealFormHelper::getMifidQuestionnaireStructure();
        $result = true;
        
        foreach ($allAnswers as $key => $answerItem)
        {
            if ( ! in_array($answerItem, $mifidQuestionStructure[$key]['answers']))
            {
                $model->addError("first_quest[{$key}]", "Proszę wybrać odpowiedź");
                $result = false;
            }
        }
        
        return $result;
    }
}