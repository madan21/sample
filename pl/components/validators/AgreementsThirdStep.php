<?php

namespace app\modules\pl\components\validators;

use app\modules\pl\models\RealForm;
use yii\validators\Validator;
use app\modules\pl\components\helpers\RealFormHelper;

/**
 * Class AgreementsThirdStep
 * @package app\modules\pl\components\validators
 *
 */
class AgreementsThirdStep extends Validator
{
    /**
     * @param RealForm $model
     * @param string $attribute
     * @return boolean
     */
    public function validateAttribute($model, $attribute)
    {
        /* @var array $acceptAgreements */
        $acceptAgreements = $model->$attribute;

        foreach (RealFormHelper::getAgreementsForThirdStep() as $key => $agreement) {
            if(!in_array($key, $acceptAgreements)) {
                $model->addError("agreements_third_step[{$key}]", "Wyrażenie zgody jest wymagane");
            }
        }
    }
}