<?php

namespace app\modules\pl\components\validators;

use app\modules\pl\components\helpers\RealFormHelper;
use yii\validators\Validator;
use app\modules\pl\models\RealForm;

class SecondStepAgreements extends Validator
{
    /**
     * @param RealForm $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $acceptAgreements = $model->$attribute;

        foreach (RealFormHelper::getAgreementsForSecondStep() as $key => $agreement) {
            if(!in_array($key, $acceptAgreements)) {
                $model->addError("agreements_second_step[{$key}]", "Wyrażenie zgody jest wymagane");
            }
        }
    }
}