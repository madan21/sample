<?php

namespace app\modules\pl\components\validators;

use app\modules\pl\components\helpers\RealFormHelper;
use yii\validators\Validator;
use app\modules\pl\models\RealForm;

/**
 * Class PersonIdDocumentNumber
 * @package app\modules\pl\components\validators
 *
 * @property string $citizenship
 * @property integer $person_id_document_type
 * @property boolean $isPolandCitizenship
 * @property boolean $documentTypeIsIdentityCard
 */
class PersonIdDocumentNumber extends Validator
{
    public $citizenship;

    public $person_id_document_type;

    /**
     * @param RealForm $model
     * @param string $attribute
     * @return boolean
     */
    public function validateAttribute($model, $attribute)
    {
        if($this->isPolandCitizenship && $this->documentTypeIsIdentityCard && !$this->validateIDCard($model->$attribute)) {
            $this->addError($model, $attribute, \Yii::t('app', 'Entered document number is of a wrong type.'));
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function getIsPolandCitizenship()
    {
        return ($this->citizenship == RealForm::POLAND_COUNTRY_CODE);
    }

    /**
     * @return bool
     */
    protected function getDocumentTypeIsIdentityCard()
    {
        return ($this->person_id_document_type == RealFormHelper::IDENTITY_CARD);
    }

    /**
     * @param string $documentNumber
     * @return bool
     */
    protected function validateIDCard($documentNumber)
    {
        $str = strtoupper(preg_replace(RealForm::DOCUMENT_NUMBER_PATTERN, '', $documentNumber));

        if (strlen($str) != 9) {
            return false;
        }

        $arrC = [
            'A' => 10,
            'B' => 11,
            'C' => 12,
            'D' => 13,
            'E' => 14,
            'F' => 15,
            'G' => 16,
            'H' => 17,
            'I' => 18,
            'J' => 19,
            'K' => 20,
            'L' => 21,
            'M' => 22,
            'N' => 23,
            'O' => 24,
            'P' => 25,
            'Q' => 26,
            'R' => 27,
            'S' => 28,
            'T' => 29,
            'U' => 30,
            'V' => 31,
            'W' => 32,
            'X' => 33,
            'Y' => 34,
            'Z' => 35
        ];

        $arrWaga = array(7, 3, 1, 7, 3, 1, 7, 3);
        $suma = 0;
        $j = 0;
        for ($i = 0; $i < strlen($str); $i++) {
            if ($i != 3) {
                if ($i < 3) {
                    $suma += $arrC[$str[$i]] * $arrWaga[$j];
                }
                else {
                    $suma += $str[$i] * $arrWaga[$j];
                }
                $j++;
            }
        }

        if (($suma % 10) == $str[3]) {
            return true;
        }
        else {
            return false;
        }
    }
}