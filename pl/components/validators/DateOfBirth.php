<?php

namespace app\modules\pl\components\validators;

use yii\validators\Validator;
use app\modules\pl\models\RealForm;

/**
 * Class DateOfBirth
 * @package app\modules\pl\components\validators
 *
 * @property boolean $no_pesel
 */
class DateOfBirth extends Validator
{
    public $no_pesel;

    /**
     * @param RealForm $model
     * @param string $attribute
     * @return boolean
     */
    public function validateAttribute($model, $attribute)
    {
        if(!$this->no_pesel) {
            return true;
        }

        if(!$this->adulthoodValidation($model->$attribute)) {
            $this->addError($model, $attribute, 'Musisz mieć ukończone 18 lat.');
            return false;
        }
    }

    /**
     * @param $dateOfBirth string
     * @return bool
     */
    protected function adulthoodValidation($dateOfBirth)
    {
        $timestampDateOfBirth = strtotime($dateOfBirth);
        $eighteenYearsTimestamp = mktime(0, 0, 0, date("m"), date("d"), date("Y") - 18);

        return $timestampDateOfBirth < $eighteenYearsTimestamp;
    }
}