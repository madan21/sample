<?php

namespace app\modules\pl\components\validators;

use app\modules\pl\models\RealForm;
use yii\validators\Validator;
use Yii;

/**
 * Class Pesel
 * @package app\modules\pl\components\validators
 *
 * @property string $citizenship
 * @property boolean $no_pesel
 */
class Pesel extends Validator
{
    public $citizenship;

    public $no_pesel;

    /**
     * @param RealForm $model
     * @param string $attribute
     * @return boolean
     */
    public function validateAttribute($model, $attribute)
    {
        if(($this->citizenship != RealForm::POLAND_COUNTRY_CODE) || $this->no_pesel) {
            return true;
        }

        if(!$this->peselValidation($model->$attribute)) {
            $this->addError($model, $attribute, 'Podano zły PESEL');
            return false;
        }

        if(!$this->adulthoodValidation($model->$attribute)) {
            $this->addError($model, $attribute, 'Musisz mieć ukończone 18 lat');
            return false;
        }
    }

    /**
     * @param $pesel string
     * @return bool
     */
    protected function peselValidation($pesel)
    {
        if (!preg_match('/^[0-9]{11}$/',$pesel)) //sprawdzamy czy ciąg ma 11 cyfr
        {
            return false;
        }

        $arrSteps = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3); // tablica z odpowiednimi wagami
        $intSum = 0;
        for ($i = 0; $i < 10; $i++)
        {
            $intSum += $arrSteps[$i] * $pesel[$i]; //mnożymy każdy ze znaków przez wagć i sumujemy wszystko
        }
        $int = 10 - $intSum % 10; //obliczamy sumć kontrolną
        $intControlNr = ($int == 10) ? 0 : $int;
        if ($intControlNr == $pesel[10]) //sprawdzamy czy taka sama suma kontrolna jest w ciągu
        {
            return true;
        }
        return false;
    }

    /**
     * @param $pesel string
     * @return bool
     */
    protected function adulthoodValidation($pesel)
    {
        $year = substr($pesel, 0, 2);
        $month = substr($pesel, 2, 2);
        $day = substr($pesel, 4, 2);

        if ($month > 12) { // gdy pesel po 2000 roku
            $month -= 20;
            $year += 2000;
        }
        else {
            $year += 1900;
        }
        $year_now = date('Y');
        $month_now = date('m');
        $day_now = date('d');


        if ((($year + 18) < $year_now) || ($year_now == ($year + 18) && $month_now > $month) || ($year_now == ($year + 18) && $month_now >= $month && $day_now >= $day)) {
            return true;
        }

        return false;
    }
}