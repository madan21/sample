<?php

namespace app\modules\pl\components\validators;

use yii\validators\Validator;
use app\modules\pl\models\RealForm;

/**
 * Class ExpiryDateIdDocument
 * @package modules\pl\components\validators
 *
 * @property boolean $no_limit_expiry_date
 */
class ExpiryDateIdDocument extends Validator
{

    public $no_limit_expiry_date;

    /**
     * @param RealForm $model
     * @param string $attribute
     * @return boolean
     */
    public function validateAttribute($model, $attribute)
    {
        if($this->no_limit_expiry_date) {
            return true;
        }

        if((strtotime($model->$attribute) < mktime(0, 0, 0, date("m"), date("d"), date("Y")))) {
            $this->addError($model, $attribute, 'Niepoprawna data ważności dowodu tożsamości');
            return false;
        }

        return true;
    }
}