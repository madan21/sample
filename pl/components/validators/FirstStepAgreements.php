<?php

namespace app\modules\pl\components\validators;

use app\modules\pl\components\helpers\RealFormHelper;
use yii\validators\Validator;
use app\modules\pl\models\RealForm;

class FirstStepAgreements extends Validator
{
    /**
     * @param RealForm $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $acceptAgreements = $model->$attribute;

        foreach (RealFormHelper::getAgreementsForFirstStep() as $key => $agreement) {
            if(!in_array($key, $acceptAgreements)) {
                $model->addError("agreements_first_step[{$key}]", "Wyrażenie zgody jest wymagane");
            }
        }
    }
}