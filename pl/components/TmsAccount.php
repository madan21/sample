<?php

namespace app\modules\pl\components;

use app\components\Request;
use app\modules\pl\components\helpers\CountryHelper;
use app\modules\pl\components\helpers\RealFormHelper;
use Tms\Components\Ws\BackOffice;
use Tms\Models\Ws\Account;
use Tms\Models\Ws\ActivationData;
use Tms\Models\Ws\ApprovalData;
use Tms\Models\Ws\Client;
use Tms\Models\Ws\Document;
use Tms\Models\Ws\IdentificationInfo;
use Tms\Models\Ws\Mifid;
use Tms\Models\Ws\MifidQuestionnaireV2;
use Tms\Models\Ws\ProfileInfo;
use Tms\Models\Ws\Resource;
use yii\base\Component;
use app\modules\pl\models\RealForm;
use Yii;
use Tms\Components\Shared\Antiflood;
use app\components\validators\AntifloodValidator;
use yii\base\Exception;
use Tms\Models\Ws\Address;
use app\modules\tms\TMS;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class TmsAccount
 * @package app\modules\pl\components
 *
 * @property RealForm $realForm
 * @property array $response
 * @property boolean $isAuthenticated
 * @property string $authEmail
 * @property TMS|null $tms
 * @property RealForm|null $cachedModel
 * @property Client $backOfficeClient
 * @property string|null $emailAccountId
 */
class TmsAccount extends Component
{
    const AUTH_TOKEN_PREFIX = 'rsid';

    const SUCCESS_STATUS = 'SUCCEEDED';

    /**
     * @var RealForm
     */
    protected $realForm;

    /**
     * @var boolean
     */
    protected $isAuthenticated = false;

    /**
     * @var TMS|null
     */
    protected $tms;

    /**
     * @var RealForm|null
     */
    protected $cachedModel;

    /**
     * @var string Adres email którym się zalogowano z real tms.pl ($_POST['rsid'])
     */
    protected $authEmail;

    /**
     * @var Client
     */
    protected $backOfficeClient;

    /**
     * @var null|string
     */
    protected $emailAccountId = null;

    /**
     * @var array
     */
    protected $response = [
        'method_result' => false,
        'errors' => [],
    ];

    /**
     * TmsAccount constructor.
     * @param RealForm $realForm
     * @param array $config
     */
    public function __construct(RealForm $realForm, array $config = [])
    {
        $this->realForm = $realForm;
        parent::__construct($config);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function validateAuthToken(Request $request)
    {
        $authToken = $request->post(self::AUTH_TOKEN_PREFIX, false);

        if(!$authToken)
        {
            $this->response['validate_token'] = true;
            $this->response['auth'] = false;
            return $this->response;
        }

        $cached = Yii::$app->cache->get(self::AUTH_TOKEN_PREFIX . $authToken);

        //check self::AUTH_TOKEN_PREFIX and check IP
        if($authToken == $cached[0] && Yii::$app->request->getUserIP() == $cached[2])
        {
            $this->authEmail = $cached[1];
            $this->isAuthenticated = true;
            $this->response['validate_token'] = true;
            $this->response['auth'] = self::AUTH_TOKEN_PREFIX . $authToken;
        }
        else
        {
            $this->response['restart'] = '1';
            $this->response['validate_token'] = false;
            Yii::$app->response->setStatusCode(403);
        }

        return $this->response;
    }

    /**
     * @return array
     */
    public function sendSms()
    {
        $debug = (YII_DEBUG && $this->realForm->phone == '+48000000000');

        $this->realForm->setScenario(RealForm::SCENARIO_STEP_ONE_PHONE);

        if($this->realForm->validate())
        {
            Yii::$app->processPoland->getDataByToken(Yii::$app->request->post('token'));
            $validator = new AntifloodValidator();
            $validator->type = Antiflood::SMS;
            $validator->init();

            if($validator->validateAttribute($this->realForm, 'phone'))
            {
                $validator->incrementCounter();
                $this->realForm->phoneVerificationCode = Yii::$app->sms->send($this->realForm->phone);
                if(Yii::$app->sms->status || $debug)
                {
                    Yii::$app->processPoland->setStepsData($this->realForm->attributes);
                    $this->response['method_result'] = true;
                    $this->response['wait'] = 1;
                    $this->response['callback'] = 'callbackSmsSend';
                    $this->response['verify_code'] = $this->realForm->phoneVerificationCode;
                }
                else
                {
                    $this->addErrorToResponse(Yii::$app->sms->errorMessage, 'phone');
                }
            }
            else
            {
                $this->addErrorToResponse($validator->message, 'phone');
            }
        }
        else
        {
            $this->addErrorToResponse($this->realForm->getErrors());
        }
        return $this->response;
    }

    /**
     * @param string $token
     * @return array
     */
    public function validateSms($token)
    {
        $debug = (YII_DEBUG && $this->realForm->phoneVerificationCode2 == '000000');

        $this->response['restart'] = false;

        try
        {
            if(Yii::$app->processPoland->getDataByToken($token))
            {

                $this->realForm->phoneVerificationCode = Yii::$app->processPoland->realForm->phoneVerificationCode;
                if($this->realForm->validate() || $debug)
                {
                    $this->realForm->phoneVerified = true;
                    Yii::$app->processPoland->setStepsData($this->realForm->attributes);
                    $this->response['method_result'] = true;
                    $this->response['wait'] = 1;
                    $this->response['callback'] = 'callbackSmsVerified';
                }
                else
                {
                    $this->addErrorToResponse($this->realForm->getErrors());
                }
            }
            else
            {
                $this->response['status'] = 'error';
                $this->response['restart'] = 1;
            }
        } catch(Exception $e)
        {
            $this->addErrorToResponse($e->getMessage());
        }

        return $this->response;
    }

    /**
     * @param string $name
     * @param string $value
     * @return array
     */
    public function validateProperty($name, $value)
    {
        $this->realForm->singleValidation($name, $value);

        $this->response = [
            'callback' => 'callbackValidated'
        ];

        if(!$this->realForm->hasErrors())
        {
            $this->response = [
                'status' => 'success',
                'wait' => 1,
                'args' => [$name, 1]
            ];
        }
        else
        {
            $this->response = [
                'status' => 'error',
                'errors' => $this->realForm->getErrors(),
                'args' => [$name, 0]
            ];

        }

        return $this->response;
    }

    /**
     * @param Request $request
     * @return array|\Tms\Models\Ws\ClientResponse|\Tms\Models\Ws\StringResponse
     */
    public function saveFirstStepData(Request $request)
    {
        $this->setTmsModule();
        Yii::$app->processPoland->getDataByToken($request->post('token'));

        if(isset(Yii::$app->processPoland->realForm->phoneVerified) && Yii::$app->processPoland->realForm->phoneVerified === true)
        {
            $this->realForm->phoneVerified = Yii::$app->processPoland->realForm->phoneVerified;
            $this->realForm->phone = Yii::$app->processPoland->realForm->phone;
        }
        if($this->realForm->validate())
        {
            $this->sendMail('real-start',$this->realForm);
            //dodanie leada
            $activationData = new ActivationData();
            $activationData->Activated = 0;
            $activationData->Name = $this->realForm->first_name;
            $activationData->Surname = $this->realForm->last_name;
            $activationData->Mail = $this->realForm->email;
            $activationData->Phone = $this->realForm->phone;
            $activationData->Id_Province = RealForm::DEFAULT_PROVINCE_ID;
            $activationData->Type = 'real_real';
            $activationData->Referer = 'tms.pl';
            $activationData->Tdpeh = '';
            $activationData->Ref = '';
            $activationData->Affid = '';
            $activationData->Tmsc = '';
            $activationData->Info = 'Rozpoczał formularz Real' . $this->realForm->getLeadSourceSuffix();
            $activationData->Source = 'TMSpl_real' . $this->realForm->getLeadSourceSuffix() . '_RODO';
            $activationData->Activation_Type = 'Real';
            $activationData->Activation_SubType = 'Real';
            $activationData->IP = $this->realForm->userIpAddress;
            $activationData->Branch_Code = $this->realForm->branch_code;
            $activationData->Language_Code = $this->realForm->language_code;

            $responseActivation = $this->tms->activation->CreateNewActivation($activationData);

            if($responseActivation->status != self::SUCCESS_STATUS)
            {
                $this->addErrorToResponse($responseActivation->errorMessage, 'activation_data');
            }
            else
            {
                //dodanie rekordu do process / wygenerowanie tokena
                $this->realForm->password = $this->generatePassword();
                Yii::$app->processPoland->activationId = $responseActivation->Value;
                Yii::$app->processPoland->setStepsData($this->realForm->attributes);
                $this->response['method_result'] = true;


                $this->addDebug('activation', $responseActivation);
            }
        }
        else
        {
            $this->addErrorToResponse($this->realForm->getErrors());
        }
        
        return $this->response;
    }

    protected function setApprovalsDataInFirstStep()
    {
        $approvalsData = new \Tms\Models\Ws\ApprovalsData();
        $approvalsData->Email = $this->cachedModel->email;
        $approvalsData->PersonId = Yii::$app->processPoland->clientId;

        foreach(RealFormHelper::getAgreementsForFirstStep() as $key => $agreement)
        {
            $agreementLabel = str_replace("\n", " ", $agreement['label']);
            $agreementLabel = preg_replace('/\s+/', ' ', trim(strip_tags($agreementLabel)));
            $approvalData = new ApprovalData();
            $approvalData->ApprovalContents = $key == 0 ? 'MARKETING_APPROVAL_MESSAGES' : 'MARKETING_APPROVAL_PHONECALLS';
            $approvalData->FormURL = Yii::$app->request->referrer;
            $approvalData->ClientIP = Yii::$app->request->getUserIP();
            $approvalData->ApprovalInput = $agreementLabel;
            $approvalsData->Approvals[] = $approvalData;
        }
        if(count($approvalsData->Approvals))
        {
            $this->tms->activation->UpsertApprovals($approvalsData);
            return true;
        }
    }


    /**
     * @param string $token
     * @return array
     */
    public function saveSecondStepData($token)
    {
        if(Yii::$app->processPoland->getDataByToken($token))
        {
            if($this->realForm->validate())
            {
                Yii::$app->processPoland->setStepsData($this->realForm->attributes);
                $this->response['method_result'] = true;
            }
            else
            {
                $this->addErrorToResponse($this->realForm->getErrors());
            }
        }
        else
        {
            $this->response['restart'] = 1;
        }

        return $this->response;
    }

    public function saveThirdStepData($token)
    {
        if(Yii::$app->processPoland->getDataByToken($token))
        {
            if($this->realForm->validate())
            {
                $this->setTmsModule();
                $this->realForm->setScenario(RealForm::SCENARIO_THIRD_STEP_POINTS);
                $this->loadPostData(RealForm::SCENARIO_THIRD_STEP_POINTS, Yii::$app->request->post());
                $this->calculateQuest();
                $this->response['args']['pointsRecommendation'] = $this->realForm->pointsRecommendation;
                $this->realForm->agreementInappropriate = Yii::$app->request->post('agreementInappropriate');
                $this->realForm->validate();
                $questIsValid = !$this->realForm->hasErrors();
                if(!$questIsValid)
                {
                    $this->response['status'] = 'error';
                    $this->response['callback'] = 'callbackPoints';
                    if($this->realForm->hasErrors())
                    {
                        $this->addErrorToResponse($this->realForm->getErrors());
                    }
                }
                else
                {
                    //step 1 and 2 data usage
                    $this->setCachedModel();

                    if(
                        $this->createClientZoneProfile() &&
                        $this->createBackOfficeClient() &&
                        $this->setApprovalsDataInFirstStep() &&
                        $this->createCRSStatement() &&
                        $this->createAccount() &&
                        $this->createFinishedRealLead()
                    )
                    {
                        $this->response['accountLogin'] = Yii::$app->processPoland->login;
                        $this->response['method_result'] = true;
                        $this->response['callback'] = 'callbackId3Check';
                        Yii::$app->processPoland->setStepsData($this->realForm->attributes);
                        $this->sendWelcomeEmail();
                        $this->sendSmsWithPassword();
                    }
                }
            }
            else
            {
                $this->addErrorToResponse($this->realForm->getErrors());
            }
        }
        else
        {
            $this->response['status'] = 'error';
            $this->response['restart'] = 1;
        }
        return $this->response;
    }

    /**
     * @param string $token
     * @return array
     */
    public function uploadDocumentFiles($token)
    {
        if(Yii::$app->processPoland->getDataByToken($token))
        {
            $this->setTmsModule();
            $this->realForm->upload_document_back_file = UploadedFile::getInstanceByName('upload_document_back_file');
            $this->realForm->upload_document_front_file = UploadedFile::getInstanceByName('upload_document_front_file');
            if($this->realForm->validate())
            {
                if(
                $this->uploadItemFileAndIncrementDocCounter('upload_document_front_file')
                )
                {
                    if(Yii::$app->processPoland->realForm->person_id_document_type == RealFormHelper::IDENTITY_CARD_PERSONAL_DOCUMENT && $this->realForm->upload_document_back_file)
                    {
                        $this->uploadItemFileAndIncrementDocCounter('upload_document_back_file');
                    }
                    $this->response['method_result'] = true;
                }
            }
            else
            {
                $this->addErrorToResponse($this->realForm->getErrors());
            }

        }
        else
        {
            $this->response['status'] = 'error';
            $this->response['restart'] = 1;
        }
        return $this->response;
    }

    protected function uploadItemFileAndIncrementDocCounter($fileName)
    {
        $result = false;
        $resource = new Resource();
        $resource->Name = $fileName . '_' . Yii::$app->processPoland->clientId . '.' . $this->realForm->$fileName->extension;
        $h = fopen($this->realForm->$fileName->tempName, "r");
        $resource->ByteArray = fread($h, filesize($this->realForm->$fileName->tempName));
        fclose($h);
        $tms = $this->tms;
        $responseUpload = $tms->resource->UploadResource($resource);
        if($responseUpload->success())
        {
            //aktualizacja konta w backoffice
            //$response = $tms->backoffice->GetClient(Yii::$app->processPoland->realForm->email);
            // $this->addDebug('client', $response);
            $client = Yii::$app->processPoland->realForm->backofficeClient;
            $client->Documents[$this->realForm->documentCounter]->FileName = $responseUpload->Value;
            $responseUpsertClient = $tms->backoffice->UpsertClient($client, null, true);
            $this->addDebug('upsert', $responseUpsertClient);
            if(!$responseUpsertClient->success())
            {
                $this->realForm->addError($fileName, $responseUpsertClient->errorMessage);
            }
            else
            {
                $this->incrementDocumentCounter();
                $result = true;
            }
        }
        else
        {
            $this->realForm->addError($fileName, $responseUpload->errorMessage);
        }

        $this->addDebug('upload', $responseUpload);

        return $result;
    }

    /**
     * @return boolean
     */
    protected function createClientZoneProfile()
    {
        $identifyResponse = $this->tms->clientZone->IdentifyEmail($this->cachedModel->email, null, 'tms');

        if(!$identifyResponse->success())
        {
            //utworzenie konta w strefie klienta
            $identifyResponse = $this->tms->clientZone->CreateProfile(
                $this->cachedModel->email,
                $this->cachedModel->password,
                $this->cachedModel->first_name,
                $this->cachedModel->last_name,
                null,
                'tms'
            );

            $this->addDebug('email_error_debug', [
                $this->cachedModel->email,
                $this->cachedModel->password,
                $this->cachedModel->first_name,
                $this->cachedModel->last_name,
                null,
                'tms'
            ]);

            if(!$identifyResponse->success())
            {
                $this->realForm->addError('email', $identifyResponse->errorMessage);
                $this->response['restart'] = '1';
                $this->addErrorToResponse($this->realForm->getErrors());
                return false;
            }

            $this->tms->clientZone->SetProfileData($identifyResponse->profileId, 'impersonation', 'CLIENT');
            $this->tms->clientZone->SetProfileData($identifyResponse->profileId, 'phone', $this->cachedModel->phone);

            $this->addDebug('identify', $this->tms->clientZone->GetProfileInfo($identifyResponse->profileId));

        }
        else
        {
            $responseProfileInfo = $this->tms->clientZone->GetProfileInfo($identifyResponse->profileId);

            $this->tms->clientZone->SetProfileData($identifyResponse->profileId, 'password', $this->cachedModel->password);
            if(!$responseProfileInfo->name)
            {
                $this->tms->clientZone->SetProfileData($identifyResponse->profileId, 'name', $this->cachedModel->first_name);
            }
            if(!$responseProfileInfo->surname)
            {
                $this->tms->clientZone->SetProfileData($identifyResponse->profileId, 'surname', $this->cachedModel->last_name);
            }
            if(!$responseProfileInfo->phone)
            {
                $this->tms->clientZone->SetProfileData($identifyResponse->profileId, 'phone', $this->cachedModel->phone);
            }
            if(!$responseProfileInfo->impersonation)
            {
                $this->tms->clientZone->SetProfileData($identifyResponse->profileId, 'impersonation', 'CLIENT');
            }

            $this->addDebug('identify', $identifyResponse);
            $this->addDebug('authIdentify', $responseProfileInfo);
        }

        if(!$identifyResponse->profileId)
        {
            $this->addErrorToResponse($identifyResponse->errorMessage, 'createClientZoneProfile');
            return false;
        }

        Yii::$app->processPoland->profileId = $identifyResponse->profileId;
        return true;
    }

    /**
     * @return boolean
     */
    protected function createBackOfficeClient()
    {
        $this->backOfficeClient = new Client();
        if($this->isAuthenticated && !empty(Yii::$app->processPoland->clientId))
        {
            $responseGetClient = $this->tms->backoffice->GetClient($this->cachedModel->email);
            $client = $responseGetClient->Client;
        }

        $this->backOfficeClient->BranchCode = RealForm::POLAND_COUNTRY_CODE;
        $this->backOfficeClient->LanguageCode = RealForm::POLAND_COUNTRY_CODE;
        $this->backOfficeClient->BusinessActivity = RealForm::BUSINESS_ACTIVITY;
        $this->backOfficeClient->Email = $this->cachedModel->email;
        $this->backOfficeClient->FirstName = $this->cachedModel->first_name;
        $this->backOfficeClient->LastName = $this->cachedModel->last_name;
        $this->backOfficeClient->Phone = $this->cachedModel->phone;
        $this->backOfficeClient->Pep = $this->cachedModel->pep;
        $this->backOfficeClient->Citizenship = $this->cachedModel->citizenship;
        $this->backOfficeClient->PersonalIdentityNumber = $this->cachedModel->pesel;
        $this->backOfficeClient->TaxResidence = $this->cachedModel->isOtherCountryTaxResident ? 0 : 1;
        $this->backOfficeClient->InlandRevenue = $this->cachedModel->poland_tax_office_id;
        $this->createPassportClientDocument($this->backOfficeClient);
        $this->incrementDocumentCounter();
        $this->setResidenceAddress($this->backOfficeClient);

        if($this->cachedModel->allowNoPesel)
        {
            $this->backOfficeClient->Birthdate = $this->cachedModel->date_of_birth;
        }

        if($this->cachedModel->ifDocumentTypeIsIdCard)
        {
            $this->createIdCardClientDocument($this->backOfficeClient);
            $this->incrementDocumentCounter();
        }

        if($this->cachedModel->issetNationalIdNumber)
        {
            $this->backOfficeClient->NationalIdentificationNumber = $this->cachedModel->national_identification_number;
        }

        $this->backOfficeClient->FATCAStatus = $this->cachedModel->usa_tax_resident;
        if($this->backOfficeClient->FATCAStatus == Client::FATCA_NATURAL_PERSON)
        {
            $this->backOfficeClient->TaxIdentificationNumberUSA = $this->cachedModel->usa_tin;
        }

        $createBackOfficeClientResponse = $this->tms->backoffice->UpsertClient($this->backOfficeClient);
        if(
            isset($createBackOfficeClientResponse->status) &&
            ($createBackOfficeClientResponse->status == self::SUCCESS_STATUS) &&
            $createBackOfficeClientResponse->Value
        )
        {
            $client_id = $createBackOfficeClientResponse->Value;
            $this->backOfficeClient->Id = $client_id;
            $this->realForm->backofficeClient = $this->backOfficeClient;
            Yii::$app->processPoland->clientId = $client_id;

            $this->addDebug('backOfficeClientResponse', $createBackOfficeClientResponse);
            return true;
        }
        else
        {
            $this->addErrorToResponse($createBackOfficeClientResponse->errorMessage, 'backoffice_upsert_client');
            return false;
        }
    }

    protected function createAccount()
    {
        if(!$this->isAuthenticated)
        {
            $psService = RealFormHelper::getTmsAccountTypeLabels()[$this->cachedModel->account_type];
            $psGroupName = null;
            $psName = mb_substr($this->cachedModel->first_name, 0, 1) . mb_substr($this->cachedModel->last_name, 0, 2);

            if($this->cachedModel->account_type == RealFormHelper::TMS_CONNECT_PREMIUM_ACCOUNT_TYPE)
            {
                $psService = 'TMS Connect';
                $psGroupName = 'Premium';
            }

            $responseCreateAccount = $this->tms->platforms->CreateAccount(
                $psService,
                $this->cachedModel->email,
                $psName,
                $this->realForm->phone,
                $this->cachedModel->password,
                $this->realForm->branch_code,
                RealFormHelper::getDepositCurrencyLabels()[$this->realForm->transfer_currency_id],
                'live',
                $psGroupName,
                100,
                0
            );

            $this->addDebug('account', $responseCreateAccount);

            if(!$responseCreateAccount->success())
            {
                $this->addErrorToResponse($responseCreateAccount->errorMessage, 'investmentGoal');
                return false;
            }
            $account = new Account();
            //$account->ServiceType = 'GO4X';
            $account->ServiceType = 'TMS Connect';
            $account->Login = $responseCreateAccount->PlatformAccount->Login;
            $account->AccountNumber = $responseCreateAccount->PlatformAccount->AccountName;
            $account->PhonePassword = $this->realForm->phone_password;
            $account->BankAccount = $this->realForm->transfer_account;
            $account->AccountCurrency = RealFormHelper::getDepositCurrencyLabels()[$this->realForm->transfer_currency_id];
            $account->DeliveryAddress = new \Tms\Models\Ws\Address();

            if($this->cachedModel->another_contact)
            {
                $account->DeliveryAddress->StreetPrefix = $this->cachedModel->street_contact;
                $account->DeliveryAddress->Street = $this->cachedModel->street_contact;
                $account->DeliveryAddress->BuildingNumber = $this->cachedModel->house_number_contact;
                $account->DeliveryAddress->HouseNumber = $this->cachedModel->apartment_number_contact;
                $account->DeliveryAddress->PostalCode = $this->cachedModel->postal_code_contact;
                $account->DeliveryAddress->City = $this->cachedModel->city_contact;
                $account->DeliveryAddress->Country = $this->cachedModel->country_contact;

            }
            else
            {
                $account->DeliveryAddress->StreetPrefix = $this->cachedModel->street_prefix;
                $account->DeliveryAddress->Street = $this->cachedModel->street;
                $account->DeliveryAddress->BuildingNumber = $this->cachedModel->house_number;
                $account->DeliveryAddress->HouseNumber = $this->cachedModel->apartment_number;
                $account->DeliveryAddress->PostalCode = $this->cachedModel->postal_code;
                $account->DeliveryAddress->City = $this->cachedModel->city;
                $account->DeliveryAddress->Country = $this->cachedModel->country;
            }

        }
        else
        {
            $getProfileInfoResponse = $this->tms->clientZone->GetProfileInfo(Yii::$app->processPoland->profileId);
            $account = array_pop($getProfileInfoResponse->accounts);
            $authAccountResponse = $this->tms->backoffice->GetAccountDetails($account);
            $account = $authAccountResponse->Account;
            Yii::$app->processPoland->login = $account->Login;
            Yii::$app->processPoland->account = $account->AccountNumber;
            $this->emailAccountId = $account->Id;

            $this->addDebug('profile', $getProfileInfoResponse);
            $this->addDebug('account', $authAccountResponse);
        }


        $account->Questionnaire->Mifid = $this->getMifidQuestionnaire();
        $account->Correspondence = $this->realForm->messaging_type;
        $account->Questionnaire->Quest10 = 'INVESTMENT_GOAL_' . $this->realForm->investment_goal;
        $account->Questionnaire->Quest11 = 'SOURCE_OF_FOUNDS_' . $this->realForm->source_of_fund;

        //bind Account object to new client/profile
        if(!$this->isAuthenticated && isset($responseCreateAccount))
        {
            $responseUpsertClient = $this->tms->backoffice->UpsertClient($this->backOfficeClient);
            if($responseUpsertClient->success())
            {
                Yii::$app->processPoland->clientId = $responseUpsertClient->Value;

                //bind account to backoffice/client
                $addAccountToClientResponse = $this->tms->backoffice->AddAccountToClient(
                    Yii::$app->processPoland->clientId,
                    $account,
                    'PLATFORMS_AGREEMENT_ONLINE'
                );

                $this->addDebug('endUpsertClient', $this->tms->backoffice->EndUpsertClient(Yii::$app->processPoland->clientId, 'TMS'));

                //bind account to platforms/clientzone
                $addAccountToProfileResponse = $this->tms->clientZone->AddAccountToProfile(
                    Yii::$app->processPoland->profileId,
                    null,
                    $addAccountToClientResponse->Value
                );

                if($addAccountToProfileResponse->hasError || $addAccountToClientResponse->hasError)
                {
                    $this->addErrorToResponse($addAccountToProfileResponse->errorMessage, 'account_profile');
                    $this->addErrorToResponse($addAccountToClientResponse->errorMessage, 'account_client');
                    return false;
                }

                $this->emailAccountId = $addAccountToClientResponse->Value;

                $this->addDebug('account2profile', $addAccountToProfileResponse);
                $this->addDebug('account2client', $addAccountToClientResponse);

                Yii::$app->processPoland->login = $responseCreateAccount->PlatformAccount->Login;
                Yii::$app->processPoland->account = $responseCreateAccount->PlatformAccount->AccountName;
            }
            else
            {
                $this->addErrorToResponse($responseUpsertClient->errorMessage, 'upsert');
                return false;
            }

            $this->addDebug('upsert', $responseUpsertClient);

        }
        else
        { //update client and account data for authenticated user
            $responseUpsertClient = $this->tms->backoffice->UpsertClient($this->backOfficeClient, $account);
            if($responseUpsertClient->hasError)
            {
                $this->addErrorToResponse($responseUpsertClient->errorMessage, 'upsert_client');
                return false;
            }
        }

        return true;
    }

    protected function createFinishedRealLead()
    {
        $this->sendMail('real-end', Yii::$app->processPoland->realForm);
        //update lead
        $activationData = new ActivationData();
        $activationData->Activated = 1;
        $activationData->Login = Yii::$app->processPoland->login;
        $activationData->Activation_Time = date('Y-m-d H:i:s');
        $firstActivationResponse = $this->tms->activation->UpdateActivation($activationData, Yii::$app->processPoland->activationId);

        //create finished real lead
        $activationData = new ActivationData();
        $activationData->Activated = 1;
        $activationData->Name = $this->cachedModel->first_name;
        $activationData->Surname = $this->cachedModel->last_name;
        $activationData->Phone = $this->cachedModel->phone;
        $activationData->Type = 'real_real';
        $activationData->Info = 'Zakończył formularz Real' . $this->cachedModel->getLeadSourceSuffix();
        $activationData->Source = 'TMSpl_real' . $this->cachedModel->getLeadSourceSuffix() . '_RODO';
        $activationData->Activation_Type = 'Real';
        $activationData->Activation_SubType = 'Connect';
        $activationData->IP = $this->cachedModel->userIpAddress;
        $activationData->Id_Province = RealForm::DEFAULT_PROVINCE_ID;
        $activationData->Referer = 'tms.pl';
        $activationData->Tdpeh = '';
        $activationData->Ref = '';
        $activationData->Affid = '';
        $activationData->Tmsc = '';
        $activationData->Mail = $this->cachedModel->email;
        $activationData->Branch_Code = $this->cachedModel->branch_code;
        $activationData->Language_Code = $this->cachedModel->language_code;
        $activationData->Parent_Activation_Key = Yii::$app->processPoland->activationId;
        $secondActivationResponse = $this->tms->activation->CreateNewActivation($activationData);

        $this->addDebug('activation', $firstActivationResponse);
        $this->addDebug('activation2', $secondActivationResponse);

        return true;
    }

    protected function sendWelcomeEmail()
    {
        //send welcome email
        $sendEmailResponse = $this->tms->backoffice->SendEmail(
            Yii::$app->processPoland->clientId,
            'CLIENT_CARD;AGREEMENT;MIFID;INFORMATION_CARD;INSTRUMENTS_CHARACTERISTIC;INSTRUMENTS_SPECIFICATION;REGULATIONS;EXECUTION_POLICY;FEES_AND_COMMISSIONS;COI',
            'WELCOME_ONLINE',
            $this->emailAccountId
        );


        $this->addDebug('sendEmail', $sendEmailResponse);
    }

    /**
     * @throws Exception
     */
    protected function createCRSStatement()
    {
        $crsStatement = new \Tms\Models\Ws\CRSStatement();
        $crsStatement->email = $this->cachedModel->email;
        $crsStatement->ClientId = Yii::$app->processPoland->clientId;
        $crsStatement->CountryOfBirth = $this->cachedModel->country_of_birth;
        $crsStatement->CityOfBirth = strip_tags($this->cachedModel->city_of_birth);
        $crsStatement->TaxResidence = $this->cachedModel->isOtherCountryTaxResident ? 0 : 1;
        if($this->cachedModel->other_country_tax_resident)
        {
            $crsDeclaration = new \Tms\Models\Ws\CRSDeclaration();
            $crsDeclaration->Country = $this->cachedModel->other_country_tax_resident_value;
            $crsDeclaration->TIN = $this->cachedModel->tin;
            if($this->cachedModel->not_have_tin_reason)
            {
                $crsDeclaration->EmptyTINReason = $this->cachedModel->not_have_tin_reason;
            }
            $crsStatement->CRSDeclarationList[] = $crsDeclaration;
        }
        $crsStatementResponse = $this->tms->backoffice->UpdateCRS($crsStatement);
        if($crsStatementResponse->success())
        {
            $responseUpdateCRS = $this->tms->backoffice->UpdateCRS($crsStatement);
            $this->addDebug('updateCrs', $responseUpdateCRS);
            return true;
        }
        else
        {
            $this->addErrorToResponse($crsStatementResponse->errorMessage, 'create_CRS_statement');
            return false;
        }
    }

    protected function sendSmsWithPassword()
    {
        Yii::$app->sms->setMessage('Hasło do rachunku: ' . $this->cachedModel->password);
        Yii::$app->sms->send($this->cachedModel->phone);
    }

    /**
     * @param Client $client
     */
    protected function setResidenceAddress(Client $client)
    {
        $client->ResidenceAddress = new Address();
        $client->ResidenceAddress->StreetPrefix = $this->cachedModel->street_prefix;
        $client->ResidenceAddress->Street = $this->cachedModel->street;
        $client->ResidenceAddress->BuildingNumber = $this->cachedModel->house_number;
        $client->ResidenceAddress->HouseNumber = $this->cachedModel->apartment_number;
        $client->ResidenceAddress->PostalCode = $this->cachedModel->postal_code;
        $client->ResidenceAddress->City = $this->cachedModel->city;
        $client->ResidenceAddress->Country = $this->cachedModel->country;

        if($this->cachedModel->isPolandCountry)
        {
            $dbSharedSiteQuery = new DbSharedSiteQuery();
            $cdp = $dbSharedSiteQuery->getFullAddressByPostalCodeAndCity($this->cachedModel->postal_code, $this->cachedModel->city);
            $dbSharedSiteQuery->closeConnection();

            if(!empty($cdp))
            {
                $client->ResidenceAddress->Commune = $cdp["commune"];
                $client->ResidenceAddress->District = $cdp["district"];
                $client->ResidenceAddress->Province = $cdp["province"];
            }
        }
    }

    /**
     * @param Client $client
     */
    protected function createPassportClientDocument(Client $client)
    {
        $client->Documents[$this->realForm->documentCounter] = new \Tms\Models\Ws\Document();
        $client->Documents[$this->realForm->documentCounter]->Type =
            $client->Documents[$this->realForm->documentCounter]->getTypesByID($this->cachedModel->person_id_document_type);
        $client->Documents[$this->realForm->documentCounter]->Number = $this->cachedModel->person_id_document_number;
        if($this->cachedModel->no_limit_expiry_date)
        {
            $client->Documents[$this->realForm->documentCounter]->ExpiryNoDate = 1;
        }
        else
        {
            $client->Documents[$this->realForm->documentCounter]->ExpiryDate = $this->cachedModel->expiry_date_person_id_document;
        }
    }

    /**
     * @param Client $client
     */
    protected function createIdCardClientDocument(Client $client)
    {
        $client->Documents[$this->realForm->documentCounter] = new \Tms\Models\Ws\Document();
        $client->Documents[$this->realForm->documentCounter]->Type = $client->Documents[0]->getTypesByID($this->cachedModel->person_id_document_type);
        $client->Documents[$this->realForm->documentCounter]->Number = $this->cachedModel->person_id_document_number;
    }

    /**
     * @param int $length
     * @return string
     */
    protected function generatePassword($length = 8)
    {
        $pass = '';
        $chars = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        while(true)
        {
            $pass .= substr(str_shuffle($chars), 0, 1);
            if($this->checkPassword($pass, $length))
            {
                break;
            }
            elseif(strlen($pass) > $length)
            {
                $pass = substr($pass, 1, $length - 1);
            }
        }
        return $pass;
    }

    /**
     * Help function for generatePassword()
     *
     * @param string $password
     * @param integer $length
     * @return bool
     */
    protected function checkPassword($password, $length)
    {
        $digit = $upper = $lower = 0;
        if(strlen($password) < $length)
        {
            return (false);
        }
        for($i = 0; $i < strlen($password); $i++)
        {
            if(ctype_digit($password[$i]))
            {
                $digit = 1;
            }
            if(ctype_lower($password[$i]))
            {
                $lower = 1;
            }
            if(ctype_upper($password[$i]))
            {
                $upper = 1;
            }
        }
        return ($digit + $upper + $lower) == 3;
    }

    /**
     * @param string $scenario
     * @param array $postData
     */
    public function loadPostData($scenario, $postData)
    {
        $this->realForm->setScenario($scenario);
        $scenarios = $this->realForm->scenarios();

        foreach($postData as $key => $value)
        {
            if(isset($scenarios[$scenario]) && in_array($key, $scenarios[$scenario]))
            {
                $this->realForm->$key = $value;
            }
        }

        $this->realForm->isAuthenticated = $this->isAuthenticated;
    }

    public function setRealFormPropertyValue($attribute, $value)
    {
        $this->realForm->$attribute = $value;
    }

    /**
     * @param string $attribute
     * @return mixed
     */
    public function getRealFormProperty($attribute)
    {
        return $this->realForm->$attribute;
    }

    /**
     * Obliczanie punktów z ankiety
     */
    protected function calculateQuest()
    {
        $mifidQuestionnaire = $this->getMifidQuestionnaire();
        $response = $this->tms->backoffice->GetMifidQuestionnaireResult($mifidQuestionnaire);
        if($response->success())
        {
            $mifid = array_pop($response->MifidList->Mifid);
            /** @var Mifid $mifid */
            $this->realForm->points = $mifid->Points;
            $this->realForm->pointsRecommendation = $mifid->Recomendation;
        }
    }

    /**
     * @return MifidQuestionnaire
     */
    protected function getMifidQuestionnaire()
    {
        $mifidQuestionnaire = new MifidQuestionnaireV2();
        $mifidQuestionnaire->HaveEducation = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_HAVE_EDUCATION);
        $mifidQuestionnaire->WorkExperience = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_WORK_EXPERIENCE);
        $mifidQuestionnaire->PlatformExperience = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_PLATFORM_EXPERIENCE);
        $mifidQuestionnaire->OrganizedMarketTrades = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_ORGANIZED_MARKET_TRADES);
        $mifidQuestionnaire->LeveragedMarketTrades = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_LEVERAGE_MARKET_TRADES);
        if($this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_LEVERAGE_MARKET_TRADES) != 'NO_TRADES')
        {
            $mifidQuestionnaire->AverageTransactionValue = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_AVERAGE_TRANSACTION_VALUE);
            $mifidQuestionnaire->NatureOfInvestment = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_NATURE_OF_INVESTMENT);
        }
        $mifidQuestionnaire->OrderTypeQuestion = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_ORDER_TYPE_QUESTION);
        $mifidQuestionnaire->InvestmentResultQuestion = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_INVEST_RESULT_QUESTION);
        $mifidQuestionnaire->HaveLossAwareness = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_HAVE_LOSS_AWARENESS);
        $mifidQuestionnaire->PurposeOfInvestment = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_PURPOSE_OF_INVESTMENT);
        $mifidQuestionnaire->InvestPercentOfSavings = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_INVEST_PERCENTAGE_OF_SAVING);
        $mifidQuestionnaire->AcceptRiskOfLoss = $this->realForm->getFirstQuestValueByKey(RealFormHelper::QUESTION_ACCEPT_RISK_OF_LOSS);

        return $mifidQuestionnaire;
    }

    protected function setTmsModule()
    {
        $this->tms = Yii::$app->getModule('tms');
    }

    protected function setCachedModel()
    {
        $this->cachedModel = Yii::$app->processPoland->realForm;
    }

    protected function incrementDocumentCounter()
    {
        $this->realForm->documentCounter++;
    }

    /**
     * @param $keyName string
     * @param $response string|array
     */
    protected function addDebug($keyName, $response)
    {
        $this->response['debug'][$keyName] = $response;
    }

    /**
     * @param $errorMessage string|array
     * @param $keyName string
     */
    protected function addErrorToResponse($errorMessage, $keyName = null)
    {
        if(is_null($keyName))
        {
            $this->response['errors'] = $errorMessage;
        }
        else
        {
            $this->response['errors'][$keyName] = $errorMessage;
        }

        $this->response['status'] = 'error';
    }

    /**
     * @return array
     */
    public function getDictionariesList()
    {
        try
        {
            $this->response = ArrayHelper::merge($this->response, [
                'agreements_for_first_step' => RealFormHelper::getAgreementsForFirstStep(),
                'agreements_for_second_step' => RealFormHelper::getAgreementsForSecondStep(),
                'agreements_for_third_step' => RealFormHelper::getAgreementsForThirdStep(),
                'document_types' => RealFormHelper::getDocumentTypeLabels(),
                'messaging_types' => RealFormHelper::getMessagingTypeLabels(),
                'tms_account_types' => RealFormHelper::getTmsAccountTypeLabels(),
                'investment_goal_types' => RealFormHelper::getInvestmentGoalTypeLabels(),
                'source_of_foundations' => RealFormHelper::getSourcesOfFundLabels(),
                'transfer_currency_id' => RealFormHelper::getDepositCurrencyLabels(),
                'countries' => CountryHelper::getList(),
                'citizenship' => CountryHelper::getCitizenshipList(),
                'mifid_questionnaire' => RealFormHelper::getMifidQuestionnaireStructure(),
                'status' => 'success',
                'method_result' => true,
            ]);
        } catch(Exception $e)
        {
            $this->addErrorToResponse($e->getMessage());
        }

        return $this->response;
    }

    /**
     * @return array
     */
    public function getProvinceAndTaxOffice($postal_code, $city = null)
    {
        try
        {

            $query = new DbSharedSiteQuery();

            $this->response = ArrayHelper::merge($this->response, [
                'provinceId' => $query->getProvinceId($postal_code),
                'taxOfficeList' => $query->getTaxOfficeList($city),
                'status' => 'success',
                'method_result' => true,
            ]);

        } catch(Exception $e)
        {
            $this->addErrorToResponse($e->getMessage());
        }

        return $this->response;
    }

    /**
     * @param string $key
     * @param array || object $data
     */
    protected function sendMail($key, $data)
    {
        $data = is_object($data)?(array)$data:$data;
        switch ($key) {
            case 'real-start':
                $data['title'] = 'Formularz rzeczywisty(mobilny)';
                $data['name'] = 'TMS Connect';
        break;
            case 'real-end':
                $data['title'] = 'Formularz rzeczywisty(mobilny)';
                $data['name'] = 'TMS Connect';
        break;
        }
        //set user email for mail template
        $data['user_email'] = $data['email'];
        //Email To
        $data['email'] = YII_DEBUG ? 'direktpoint@tms.pl' : 'sales@tms.pl';
        Yii::$app->mailer->sendMail($key, $data );

    }
}
